/* global __dirname */
var path = require('path');

module.exports = {
  entry: {
    react: './src/main/js/react.jsx'
  },
  devtool: 'sourcemaps',
  cache: true,
  mode: 'development',
  output: {
    path: path.resolve(__dirname, 'src', 'main', 'resources', 'static', 'build')
  },
  resolve:{
    extensions: ['.js', '.jsx'],
    alias:{
      css: path.resolve(__dirname, "src", "main", "css"),
      components: path.resolve(__dirname, "src", "main", "components"),
      context: path.resolve(__dirname, "src", "main", "context"),
      hooks: path.resolve(__dirname, "src", "main", "hooks")
    }
  },
  module: {
    rules: [{ // babel module
      test: path.join(__dirname, '.'),
      exclude: /(node_modules)/,
      use: [{
        loader: 'babel-loader',
        options: {
          presets: ["@babel/preset-env", "@babel/preset-react"]
        }
      }]
    }, { //scss processing
      test: /\.(scss)$/,
      use: [{
        loader: 'style-loader' // inject CSS to page
      }, {
        loader: 'css-loader' // translates CSS into CommonJS modules
      }, {
        loader: 'postcss-loader', // Run postcss actions
        options: {
          plugins: function() { // postcss plugins, can be exported to postcss.config.js
            return [require('autoprefixer')];
          }
        }
      }, {
        loader: 'sass-loader' // compiles Sass to CSS
      }]
    }]
  },
  optimization: {
    splitChunks: {
      cacheGroups: {
        vendor: {
          test: /node_modules/,
          chunks: "initial",
          name: "vendor",
          enforce: true
        }
      }
    }
  }
};