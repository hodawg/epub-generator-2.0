import React, {useState, useEffect} from 'react';
import {useParams} from 'react-router-dom';
import {useHistory} from 'react-router-dom';
import axios from 'axios';

import Accordion from 'react-bootstrap/Accordion';
import Card from 'react-bootstrap/Card';
import TabContainer from 'react-bootstrap/TabContainer';
import TabContent from 'react-bootstrap/TabContent';
import TabPane from 'react-bootstrap/TabPane';
import Nav from 'react-bootstrap/Nav';
import Modal from 'react-bootstrap/Modal';

import {useErrorDispatch} from 'context/ErrorContext';
import {useUrlDispatch, useUrlState} from 'context/UrlContext';
import {useUpdateState} from 'context/UpdateContext';
import Properties from './EditProject/Properties';
import Edits from './EditProject/Edits';
import Images from './EditProject/Images';
import Logs from './EditProject/Logs';
import TagInfo from './EditProject/TagInfo';
import Css from './EditProject/Css';

export default function EditProject() {
  const setError = useErrorDispatch();
  const history = useHistory();
  const urls = useUrlState();
  const setUrls = useUrlDispatch();
  const lastUpdate = useUpdateState();
  const {id} = useParams();
  const [showConfirm, setShowConfirm] = useState(false);
  const [project, setProject] = useState(null);
  const [scan, setScan] = useState(null);
  const url = urls.projects.href + '/' + id;

  useEffect(() => {
    axios.get(url)
      .then(res => setProject(res.data))
      .catch(e => setError('EditProject (useEffect1):' + e.message));
  }, []);

  useEffect(() => {
    if (project !== null) {
      axios.get(project._links.scan.href)
        .then(res => setScan(res.data))
        .catch(e => setError('EditProject (useEffect2):' + e.message));
    }
  }, [project, lastUpdate]);

  const deleteProject = () => {
    axios.delete(url)
      .then(() => history.push('/'))
      .catch(e => setError('EditProject (deleteProject):' + e.message));
  }

  const generateEpub = () => {
    axios.get(project._links.generate.href)
      .then(res => window.open(res.data._links.download.href, '_blank'))
      .catch(e => setError('EditProject (generateEpub):' + e.message));
  }

  return (
    <div className="form-row">
      <div className="col-3">
        <Accordion className="side-bar" defaultActiveKey="properties">
          <Card>
            <Accordion.Toggle as={Card.Header} eventKey="properties">
              <Card.Title className="mb-0">Properties</Card.Title>
            </Accordion.Toggle>
            <Accordion.Collapse eventKey="properties">
              <Card.Body>
                {project === null ? '' : <Properties project={project} />}
              </Card.Body>
            </Accordion.Collapse>
          </Card>
          <Card>
            <Accordion.Toggle as={Card.Header} eventKey="edits">
              <Card.Title className="mb-0">Edits</Card.Title>
            </Accordion.Toggle>
            <Accordion.Collapse eventKey="edits">
              <Card.Body>
                {project === null ? '' : <Edits project={project} />}
              </Card.Body>
            </Accordion.Collapse>
          </Card>
          <Card>
            <Accordion.Toggle as={Card.Header} eventKey="images">
              <Card.Title className="mb-0">Images</Card.Title>
            </Accordion.Toggle>
            <Accordion.Collapse eventKey="images">
              <Card.Body>
                {project === null ? '' : <Images project={project} />}
              </Card.Body>
            </Accordion.Collapse>
          </Card>
        </Accordion>
      </div>
      <div className="col-9">
        <TabContainer defaultActiveKey="scan">
          <div className="d-flex justify-content-between align-items-center">
            <Nav variant="pills" className="top-bar">
              <Nav.Item><Nav.Link eventKey="scan">Scan</Nav.Link></Nav.Item>
              <Nav.Item><Nav.Link eventKey="log">Log</Nav.Link></Nav.Item>
              <Nav.Item><Nav.Link eventKey="preview">Preview</Nav.Link></Nav.Item>
              <Nav.Item><Nav.Link eventKey="source">Source</Nav.Link></Nav.Item>
              <Nav.Item><Nav.Link eventKey="css">CSS</Nav.Link></Nav.Item>
            </Nav>
            <div>
              <button type="button" className="btn btn-outline-danger" onClick={() => setShowConfirm(true)}>Delete</button>
              <button type="button" className="btn btn-outline-primary ml-4" onClick={() => generateEpub()}>Generate</button>
              <a href="/" className="btn btn-outline-primary ml-4">Back</a>
            </div>
          </div>
          <TabContent>
            <TabPane eventKey="scan">
              <div className="tab-body">
                <div className="form-row">
                  <div className="col-12">
                    <div className="list-group">
                      {scan ? scan.tags.map(ti => <TagInfo key={ti.name} tagResult={ti} />) : ''}
                    </div>
                  </div>
                </div>
              </div>
            </TabPane>
            <TabPane eventKey="log">
              <div className="tab-body">
                <div className="form-row">
                  {scan ? <Logs logs={scan.logs}/> : ''}
                </div>
              </div>
            </TabPane>
            <TabPane eventKey="preview">
              <div className="tab-body">
                <div className="form-row">
                  {scan ? <div className="col-12" dangerouslySetInnerHTML={{ __html: scan.preview }}></div> : ''}
                </div>
              </div>
            </TabPane>
            <TabPane eventKey="source">
              <div className="tab-body">
                <div className="form-row">
                  <div className="col-12">
                    <pre>
                      {scan ? <code dangerouslySetInnerHTML={{ __html: scan.source }}></code> : ''}
                    </pre>
                  </div>
                </div>
              </div>
            </TabPane>
            <TabPane eventKey="css">
              <div className="tab-body">
                <Css />
              </div>
            </TabPane>
          </TabContent>
        </TabContainer>
      </div>

      <Modal show={showConfirm} onHide={() => setShowConfirm(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Are you sure?</Modal.Title>
        </Modal.Header>
        <Modal.Body>This action can not be undone!</Modal.Body>
        <Modal.Footer>
          <button type="button" className="btn btn-outline-danger" onClick={() => deleteProject()}>Delete</button>
          <button type="button" className="btn btn-outline-secondary" onClick={() => setShowConfirm(false)}>Cancle</button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};