import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';
import Alert from 'react-bootstrap/Alert';

import {useErrorDispatch} from 'context/ErrorContext';
import {useUrlState} from 'context/UrlContext';
import NewProject from './ListProjects/NewProject';
import CopyProject from './ListProjects/CopyProject';

export default function ListProjects(props) {
  const setError = useErrorDispatch();
  const urls = useUrlState();
  const [projects, setProjects] = useState([]);
  const [stats, setStats] = useState([]);
  const url = props.url;

  useEffect(() => {
    axios.get(url)
      .then(res => setProjects(res.data._embedded.projects))
      .catch(e => setError('ListProjects (useEffect 1): ' + e.message));
  }, []);

  useEffect(() => {
    if (urls.projectStats) {
      axios.get(urls.projectStats.href)
        .then(res => setStats(res.data))
        .catch(e => setError('ListProjects (useEffect 2): ' + e.message));
    }
  }, [urls]);

  return (
    <div className="form-row">
      <div className="col-8">
        <div className="card">
          <div className="card-header">
            <div className="d-flex justify-content-between align-items-center">
              <h3 className="text-primary m-0 w-25">Projects</h3>
              <NewProject />
            </div>
          </div>
          <div className="list-group list-group-flush">
            {projects.map(p => (
              <div key={p.id} className="list-group-item">
                <div className="d-flex justify-content-between align-items-center">
                  <Link to={'/project/' + p.id} className="btn btn-outline-primary w-25">{p.title}</Link>
                  <CopyProject url={p._links.copy.href} />
                </div>
              </div>
             ))}
          </div>
        </div>
      </div>
      <div className="col-4">
        <div className="card">
          <div className="card-header">
            <h3 className="text-primary m-0">Stats</h3>
          </div>
          <div className="list-group list-group-flush">
            {stats.map(s => (
              <div key={s.label} className="list-group-item">
                <div className="d-flex justify-content-between align-items-center">
                  <strong className="text-primary">{s.label}</strong>
                  <span>times used: {s.value}</span>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};