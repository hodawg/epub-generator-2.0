import React, {useState, useEffect} from 'react';
import {useHistory} from 'react-router-dom';
import axios from 'axios';
import {useErrorDispatch} from 'context/ErrorContext';
import {useUrlState} from 'context/UrlContext';

export default function NewProject() {
  const setError = useErrorDispatch();
  const urls = useUrlState();
  const [newTitle, setNewTitle] = useState('');

  let history = useHistory();
  let buttonClass = 'mb-0 btn btn-block btn-outline-primary' + (newTitle ? '' : ' disabled');

  const newProject = (files) => {
    let formData = new FormData();
    formData.append('title', newTitle);
    formData.append('file', files[0]);

    axios.post(urls.newProject.href, formData)
      .then(res => history.push('/project/' + res.data.id))
      .catch(e => setError('NewProject (newProject): ' + e.message));
  }

  return (
    <div className="input-group w-30">
      <input type="text" className="form-control" placeholder="New Project Title" value={newTitle} onChange={e => setNewTitle(e.target.value)} />
      <div className="input-group-append">
        <label className={buttonClass}>
          <span>Choose File</span>
          <input type="file" accept="text/plain" multiple={false} hidden onChange={e => newProject(e.target.files)} disabled={!newTitle}/>
        </label>
      </div>
    </div>
  );
};