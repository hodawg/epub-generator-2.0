import React, {useState, useEffect} from 'react';
import axios from 'axios';
import Modal from 'react-bootstrap/Modal';

import {useErrorDispatch} from 'context/ErrorContext';
import {useUrlState} from 'context/UrlContext';
import {useUpdateDispatch} from 'context/UpdateContext';
import TextInput from 'components/TextInput';
import ImageEdit from './Images/ImageEdit';

export default function ImageEditsBody(props) {
  const setError = useErrorDispatch();
  const triggerChange = useUpdateDispatch();
  const [showConfirm, setShowConfirm] = useState(false);

  const [imageFiles, setImageFiles] = useState([]);
  const [coverId, setCoverId] = useState(props.project.coverId);

  const urls = useUrlState();
  const associationUrl = props.project._links.imageFiles.href;
  const projectUrl = props.project._links.self.href;

  let addFilesBtnLbl = imageFiles.length > 0 ? imageFiles.length + ' Files Selected' : 'Choose Files';

  useEffect(() => {
    axios.get(associationUrl)
      .then(res => setImageFiles(res.data._embedded.imageFiles))
      .catch(e => setError('ImageEditsBody (useEffect): ' + e.message));
  }, []);

  const saveCoverId = value => {
    axios.patch(projectUrl, {coverId: value})
      .then(() => {
        setCoverId(value);
        triggerChange();
      })
      .catch(e => setError('ImageEditsBody (saveCoverId): ' + e.message));
  }

  const saveImages = files => {
    let formData = new FormData();
    Array.from(files).forEach(f => formData.append('files', f));

    axios.post(urls.imageFiles.href, formData)
      .then(res => {
        setImageFiles(res.data._embedded.imageFiles);

        let headers = {headers: {'Content-Type': 'text/uri-list'}};
        let body = res.data._embedded.imageFiles.map(f => f._links.self.href).join('\n');
        return axios.post(associationUrl, body, headers);
      })
      .then(() => triggerChange())
      .catch(e => setError('ImageEditsBody (saveImages): ' + e.message));
  };

  const deleteImages = () => {
    let deletes = imageFiles.map(f => axios.delete(f._links.self.href));
    axios.all(deletes)
      .then(() => {
        setCoverId(0);
        setImageFiles([]);
        setShowConfirm(false);
        triggerChange();
      })
      .catch(e => setError('ImageEditsBody (deleteImages): ' + e.message));
  };

  return (
    <div className="form-row align-items-start">
      <div className="col-6">
        <div className="form-group">
          <label>Add Files</label>
          <label className="btn btn-block btn-outline-info">
            <span>{addFilesBtnLbl}</span>
            <input type="file" accept="image/*" multiple={true} hidden onChange={e => saveImages(e.target.files)} />
          </label>
        </div>
      </div>
      <div className="col-3">
        <TextInput name="imageAttr" value={props.project.imageAttr} url={projectUrl}>
          <label>Image Attr</label>
        </TextInput>
      </div>
      <div className="col-3">
        <div className="form-group">
          <label>&nbsp;</label>
          <button type="button" className="btn btn-outline-danger btn-block" onClick={() => setShowConfirm(true)}>Clear All</button>
        </div>
      </div>
      <div className="col-12">
        <div className="list-group">
          {imageFiles.map(i => (
            <ImageEdit key={i._links.self.href} image={i} value={coverId} onClick={id => saveCoverId(id)} />
          ))}
        </div>
      </div>

      <Modal show={showConfirm} onHide={() => setShowConfirm(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Are you sure?</Modal.Title>
        </Modal.Header>
        <Modal.Body>This action can not be undone!</Modal.Body>
        <Modal.Footer>
          <button type="button" className="btn btn-outline-danger" onClick={() => deleteImages()}>Delete</button>
          <button type="button" className="btn btn-outline-secondary" onClick={() => setShowConfirm(false)}>Cancle</button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};