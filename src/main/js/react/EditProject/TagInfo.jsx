import React, {useState} from 'react';
import Collapse from 'react-bootstrap/Collapse';

function ActionResult(props) {
  const [values, setValues] = useState(props.attr.values);
  const [prefix, setPrefix] = useState('');

  const getCombindedValue = v => {
    return prefix + (prefix !== '' && v.startsWith('..') ? v.substring(2) : v);
  };

  const open = () => {
    values.forEach(v => window.open(getCombindedValue(v), '_blank'));
  };

  return (
    <div className="card">
      <div className="card-body">
        {props.attr.name === 'src' || props.attr.name === 'href' ? (
          <div className="input-group w-50 mb-3">
            <input type="text" className="form-control" placeholder="prefix" value={prefix} onChange={e => setPrefix(e.target.value)}/>
            <div className="input-group-append">
              <button type="button" className="btn btn-outline-primary" onClick={() => open()}>Open</button>
            </div>
          </div>
        ) : ''}
        <h5>{props.attr.count} occurrence(s) of attribute: {props.attr.name}</h5>
        {props.attr.values.map(v => <h6 key={v} className="h-ellipsis pl-3">{getCombindedValue(v)}</h6>)}
      </div>
    </div>
  );
}

export default function TagResult(props) {
  const hasAttr = props.tagResult.attributes.length > 0;
  const btnClass = 'btn btn-outline-primary btn-block' + (hasAttr ? '' : ' disabled');
  const [open, setOpen] = useState(false);

  return (
    <div className="list-group-item">
      <button className={btnClass} disabled={!hasAttr} onClick={() => setOpen(!open)}>{props.tagResult.count} occurrence(s) of: &lt;{props.tagResult.name}&gt;</button>
      <Collapse in={open}>
        <div>{props.tagResult.attributes.map(a => <ActionResult key={a.name} tagName={props.tagResult.name} attr={a} />)}</div>
      </Collapse>
    </div>
  );
}