import React, {useState, useEffect} from 'react';
import axios from 'axios';

import {useErrorDispatch} from 'context/ErrorContext';
import {useUrlState} from 'context/UrlContext';

function Log(props) {
  return (
    <div className="list-group-item">
      <div className="form-row">
        <div className="col-2 text-nowrap">{props.log.level}<br/>{props.log.action}<br/>{props.log.mutator}</div>
        <div className="col-10">{props.log.message}</div>
      </div>
    </div>
  );
}

export default function Logs(props) {
  const urls = useUrlState();
  const [levels, setLevels] = useState(['INFO', 'WARNING', 'ERROR']);
  const [actions, setActions] = useState(['SELECT', 'PARAMS', 'INSERT', 'REPLACE', 'REMOVE']);
  const [mutators, setMutators] = useState([]);
  const [selected, setSelected] = useState([]);

  useEffect(() => {
    axios.get(urls.editFunctions.href)
      .then(res => setMutators(res.data))
      .catch(e => setError('Logs (useEffect):' + e.message));
  }, []);

  const filterLogs = log => {
    if (selected.length === 0) {
      return true;
    }
    return selected.includes(log.level) || selected.includes(log.action) || selected.includes(log.mutator);
  };

  const getButtonClass = value => {
    if (selected.includes(value)) {
      return 'btn btn-outline-secondary active';
    }
    return 'btn btn-outline-secondary';
  };

  const toggleSelected = value => {
    var temp = selected.slice();
    if (temp.includes(value)) {
      temp.splice(temp.indexOf(value), 1);
    }
    else {
      temp.push(value);
    }
    setSelected(temp);
  };

  return (
    <div className="col-12">
      <div className="row">
        <div className="col-12 mb-2">
          <div className="btn-toolbar">
            <div className="btn-group btn-group-xs mr-2">
              {levels.map(l => <button key={l} type="button" className={getButtonClass(l)} onClick={() => toggleSelected(l)}>{l.toLowerCase()}</button>)}
            </div>
            <div className="btn-group btn-group-xs mr-2">
              {actions.map(a => <button key={a} type="button" className={getButtonClass(a)} onClick={() => toggleSelected(a)}>{a.toLowerCase()}</button>)}
            </div>
            <div className="btn-group btn-group-xs">
              {mutators.map(m => <button key={m.name} type="button" className={getButtonClass(m.label)} onClick={() => toggleSelected(m.label)}>{m.label}</button>)}
            </div>
          </div>
        </div>
        <div className="col-12">
          <div className="list-group">
            {props.logs.filter(log => filterLogs(log)).map(log => <Log key={log.id} log={log}/>)}
          </div>
        </div>
      </div>
    </div>
  );
}