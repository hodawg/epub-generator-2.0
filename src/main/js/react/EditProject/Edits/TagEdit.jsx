import React, {useState, useEffect} from 'react';
import axios from 'axios';

import {useErrorDispatch} from 'context/ErrorContext';
import {useUpdateDispatch} from 'context/UpdateContext';
import TextInput from 'components/TextInput';
import Params from './Params';

export default function TagEdit(props) {
  const setError = useErrorDispatch();
  const triggerChange = useUpdateDispatch();
  const [skip, setSkip] = useState(props.edit.skip);
  const editFunction = props.edit.editFunction;
  const editUrl = props.edit._links.self.href;

  const toggleSkip = () => {
    axios.patch(editUrl, {skip: !skip})
      .then(() => {
        setSkip(!skip);
        triggerChange();
      })
      .catch(e => setError('TagEdit (toggleSkip): ' + e.message));
  };

  const deleteEdit = () => {
    axios.delete(editUrl)
      .then(() => {
        triggerChange();
        props.onDelete();
      })
      .catch(e => setError('TagEdit (deleteEdit): ' + e.message));
  };

  let skipClass = 'btn btn-xs btn-outline-info' + (skip ? ' active' : '');
  let deleteClass = 'ml-1 btn btn-xs btn-outline-danger' + (props.canDelete ? '' : ' d-none');

  return (
    <div className="list-group-item">
      <div className="pb-1 w-100 d-flex justify-content-between align-items-center">
        <label className="mb-0">{editFunction.label}</label>
        <div>
          <button type="button" className={skipClass} onClick={() => toggleSkip()}>skip</button>
          <button type="button" className={deleteClass} onClick={() => deleteEdit()}>delete</button>
        </div>
      </div>
      <TextInput name="selector" placeholder="selector" className="mb-1" value={props.edit.selector} url={editUrl} />
      <Params params={props.edit.params} editFunction={editFunction} url={editUrl} />
    </div>
  );
};