import React, {useState, useEffect} from 'react';
import axios from 'axios';

import {useErrorDispatch} from 'context/ErrorContext';
import {useUpdateDispatch} from 'context/UpdateContext';
import {useUpdate} from 'hooks/useUpdate';

export default function Params(props) {
  const setError = useErrorDispatch();
  const triggerChange = useUpdateDispatch();
  const [params, setParams] = useState(props.params);
  const editFunction = props.editFunction;
  const editUrl = props.url;

  useUpdate(() => {
    const timer = setTimeout(() => {
      axios.patch(editUrl, {params: params})
        .then(() => triggerChange())
        .catch(e => setError('Params (useUpdate): ' + e.message));
    }, 1000);
    return () => clearTimeout(timer);
  }, [params]);

  const mapToParams = (key, value) => {
    setParams(params.map(p => p.key === key ? {key: key, value: value} : p));
  };

  const getParamValue = key => {
    return params.filter(p => p.key === key).map(p => p.value)[0];
  };

  return (
    <div>
      {Object.entries(editFunction.params).map(([key, value]) => {
        return (
          <div key={key} className="form-group mb-1">
            <input type="text" className="form-control" placeholder={value} value={getParamValue(key)} onChange={e => mapToParams(key, e.target.value)} />
          </div>
        );
      })}
    </div>
  );
}