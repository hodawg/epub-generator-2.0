import React, {useState, useEffect} from 'react';
import axios from 'axios';

import {useErrorDispatch} from 'context/ErrorContext';
import {useUpdate} from 'hooks/useUpdate';
import {useUrlState} from 'context/UrlContext';
import TextareaInput from 'components/TextareaInput';

export default function Css() {
  const setError = useErrorDispatch();
  const urls = useUrlState();
  const [css, setCss] = useState(null);
  const [cssUrl, setCssUrl] = useState(null);

  useEffect(() => {
    axios.get(urls.cssFiles.href)
      .then(res => {
        setCss(res.data._embedded.cssFiles[0].css);
        setCssUrl(res.data._embedded.cssFiles[0]._links.self.href);
      })
      .catch(e => setError('Css (useEffect): ' + e.message));
  }, []);

  return (
    <div className="form-row">
      <div className="col-12">
        <h3 className="text-warning">Please remember this is a global css file.</h3>
        {((css || css === '') && cssUrl) ? <TextareaInput name="css" method="put" value={css} url={cssUrl} /> : ''}
      </div>
    </div>
  );
}