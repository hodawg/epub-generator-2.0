import React from 'react';

import TextInput from 'components/TextInput';
import TextareaInput from 'components/TextareaInput';
import TextFileInput from './Properties/TextFileInput';
import EncodingSelect from './Properties/EncodingSelect';

export default function Properties(props) {
  const projectUrl = props.project._links.self.href;
  const project = props.project;

  return (
    <div className="form-row">
      <div className="col-12">
        <TextInput name="title" value={project.title} url={projectUrl}>
          <label>Title</label>
        </TextInput>
      </div>
      <div className="col-12">
        <TextInput name="author" value={project.author} url={projectUrl}>
          <label>Author</label>
        </TextInput>
      </div>
      <div className="col-4">
        <TextInput name="volume" value={project.volume} url={projectUrl}>
          <label>Volume</label>
        </TextInput>
      </div>
      <div className="col-4">
        <TextInput name="chapterTag" value={project.chapterTag} url={projectUrl}>
          <label>Chapter Tag</label>
        </TextInput>
      </div>
      <div className="col-4">
        <TextInput name="partTag" value={project.partTag} url={projectUrl}>
          <label>Part Tag</label>
        </TextInput>
      </div>
      <div className="col-8">
        <TextFileInput url={project._links.textFile.href}/>
      </div>
      <div className="col-4">
        <EncodingSelect name="encoding" value={project.encoding} url={projectUrl}>
          <label>File Encoding</label>
        </EncodingSelect>
      </div>
      <div className="col-12">
        <TextareaInput name="notes" value={project.notes} url={projectUrl}>
          <label>Notes</label>
        </TextareaInput>
      </div>
    </div>
  );
};