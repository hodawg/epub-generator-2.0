import React, {useState, useEffect} from 'react';
import axios from 'axios';

import {useErrorDispatch} from 'context/ErrorContext';
import {useUrlState} from 'context/UrlContext';
import {useUpdateDispatch} from 'context/UpdateContext';
import TagEdit from './Edits/TagEdit';

export default function Edits(props) {
  const setError = useErrorDispatch();
  const triggerChange = useUpdateDispatch();
  const [canDelete, setCanDelete] = useState(false);
  const [editTrigger, updateEdits] = useState(new Date());
  const [editFunctions, setEditFunctions] = useState([]);
  const [edits, setEdits] = useState([]);
  const urls = useUrlState();
  const associationUrl = props.project._links.tagEdits.href

  useEffect(() => {
    axios.get(urls.editFunctions.href)
      .then(res => setEditFunctions(res.data))
      .catch(e => setError('Edits (useEffect): ' + e.message));
  }, []);

  useEffect(() => {
    axios.get(associationUrl)
      .then(res => {
        setEdits(res.data._embedded.tagEdits);
        setCanDelete(false);
      })
      .catch(e => setError('Edits (useEffect): ' + e.message));
  }, [editTrigger]);

  const newDocumentEdit = select => {
    let selectedFunction = editFunctions.filter(ef => ef.name === select.value)[0];
    let params = Object.keys(selectedFunction.params).map(k => ({key: k, value: ''}));

    axios.post(urls.tagEdits.href, {selector: '', editFunction: select.value, params: params, skip: false})
      .then(res => axios.post(associationUrl, res.data._links.self.href, {headers: {'Content-Type': 'text/uri-list'}}))
      .then(() => {
        updateEdits(new Date());
        select.value = '';
        triggerChange();
      })
      .catch(e => setError('Edits (newDocumentEdit): ' + e.message));
  };

  let removeBtnClass = 'btn btn-outline-danger' + (canDelete ? ' active' : '');

  return (
    <div className="form-row align-items-start">
      <div className="col-12">
        <div className="form-group">
          <div className="w-100 d-flex justify-content-between align-items-end">
            <label>Add Edit</label>
            <small className="mb-2">Note: Edits in reverse order.</small>
          </div>
          <div className="input-group">
            <select className="form-control" onChange={e => newDocumentEdit(e.target)}>
              <option></option>
              {editFunctions.map(o => <option key={o.name} value={o.name}>{o.label}</option>)}
            </select>
            <div className="input-group-append">
              <button type="button" className={removeBtnClass} onClick={() => setCanDelete(!canDelete)}>Remove</button>
            </div>
          </div>
        </div>
      </div>
      <div className="col-12">
        <div className="list-group">
          {edits.slice().reverse().map(e => <TagEdit key={e.id} edit={e} canDelete={canDelete} onDelete={() => updateEdits(new Date())} />)}
        </div>
      </div>
    </div>
  );
};