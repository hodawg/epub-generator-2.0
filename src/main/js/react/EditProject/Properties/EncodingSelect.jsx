import React, {useState, useEffect} from 'react';
import axios from 'axios';

import {useErrorDispatch} from 'context/ErrorContext';
import {useUrlState} from 'context/UrlContext';
import {useUpdateDispatch} from 'context/UpdateContext';

export default function EncodingSelect(props) {
  const setError = useErrorDispatch();
  const urls = useUrlState();
  const triggerChange = useUpdateDispatch();
  const [value, setValue] = useState(props.value.name);
  const name = props.name;
  const url = props.url;

  const [options, setOptions] = useState([]);

  useEffect(() => {
    axios.get(urls.fileEncodings.href)
      .then(res => setOptions(res.data))
      .catch(e => setError('EncodingSelect (useEffect): ' + e.message));
  }, []);

  const saveEncoding = value => {
    axios.patch(url, {[name]: value})
      .then(() => {
        setValue(value);
        triggerChange();
      })
      .catch(e => setError('EncodingSelect (saveEncoding): ' + e.message));
  }

  return (
    <div className="form-group">
      {props.children}
      <select className="form-control" value={value} onChange={e => saveEncoding(e.target.value)}>
        {options.map(o => <option key={o.name} value={o.name}>{o.label}</option>)}
      </select>
    </div>
  );
};