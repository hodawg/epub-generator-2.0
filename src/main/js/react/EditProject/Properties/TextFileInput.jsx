import React, {useState, useEffect} from 'react';
import axios from 'axios';

import {useErrorDispatch} from 'context/ErrorContext';
import {useUpdateDispatch} from 'context/UpdateContext';

export default function TextFileInput(props) {
  const setError = useErrorDispatch();
  const triggerChange = useUpdateDispatch();
  const [btnLabel, setBtnLabel] = useState('Choose File');
  let [selfUrl, setSelfUrl] = useState('');

  useEffect(() => {
    axios.get(props.url)
      .then(res => {
        setSelfUrl(res.data._links.self.href);
        setBtnLabel(res.data.fileName ? res.data.fileName : 'Choose File');
      })
      .catch(e => setError('TextFileInput (useEffect): ' + e.message));
  }, []);

  const saveTextFile = files => {
    let formData = new FormData();
    Array.from(files).forEach(f => formData.append('file', f));

    axios.put(selfUrl, formData)
      .then(res => {
        setBtnLabel(res.data.fileName ? res.data.fileName : 'Choose File');
        triggerChange();
      })
      .catch(e => setError('TextFileInput (saveTextFile): ' + e.message));
  }

  return (
    <div className="form-group">
      <label>Text File</label>
      <label className="btn btn-block btn-outline-info">
        <span>{btnLabel}</span>
        <input type="file" accept="text/plain" multiple={false} hidden onChange={e => saveTextFile(e.target.files)}/>
      </label>
    </div>
  );
};