import React, {useState, useEffect} from 'react';
import axios from 'axios';

import TextInput from 'components/TextInput';
import {useUpdateDispatch} from 'context/UpdateContext';

export default function ImageEdit(props) {
  const triggerChange = useUpdateDispatch();
  const [name, setName] = useState(props.image.fileName);
  const [size, setSize] = useState(props.image.fileSize);
  const [width, setWidth] = useState(props.image.width);
  const [height, setHeight] = useState(props.image.height);
  const imageUrl = props.image._links.self.href;

  let btnClass = 'btn btn-outline-info' + (props.value === props.image.id ? ' active' : '');

  const patchImageFile = files => {
    let formData = new FormData();
    Array.from(files).forEach(f => formData.append('file', f));

    axios.patch(imageUrl, formData)
      .then(res => {
        setName(res.data.fileName);
        setSize(res.data.fileSize);
        setWidth(res.data.width);
        setHeight(res.data.height);
        triggerChange();
      });
  }

  return (
    <div className="list-group-item">
      <div className="d-flex flex-row mb-1">
        <TextInput name="searchName" type="form" className="flex-fill mb-0 mr-1" placeholder="search name" value={props.image.searchName} url={imageUrl} />
        <button className={btnClass} onClick={() => props.onClick(props.image.id)}>Cover</button>
      </div>
      <label className="btn btn-block btn-outline-info">
        <span>{name}</span>
        <input type="file" accept="image/*" multiple={false} hidden onChange={e => patchImageFile(e.target.files)}/>
      </label>
      <div className="d-flex w-100 justify-content-between align-items-center">
        <small><span className="font-weight-bold">size:</span> {(size / 1000).toFixed(2)}kb</small>
        <small><span className="font-weight-bold">width:</span> {width}px</small>
        <small><span className="font-weight-bold">height:</span> {height}px</small>
      </div>
    </div>
  );
};