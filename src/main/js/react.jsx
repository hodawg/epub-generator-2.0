import 'css/style.scss';
import ReactDOM from 'react-dom';
import React, {useEffect} from 'react';
import {HashRouter, Switch, Route} from 'react-router-dom';
import axios from 'axios';

import {ErrorProvider, useErrorState} from 'context/ErrorContext';
import {UrlProvider, useUrlDispatch, useUrlState} from 'context/UrlContext';
import {UpdateProvider} from 'context/UpdateContext';
import ListProjects from './react/ListProjects';
import EditProject from './react/EditProject';

function Main() {
  const error = useErrorState();
  const setUrl = useUrlDispatch();
  const urls = useUrlState();

  useEffect(() => {
    axios.get('/api').then(res => setUrl(res.data._links));
  }, []);

  return (
      <HashRouter>
        <div className="container-fluid pt-3">
          {error ? <div className="alert alert-danger" role="alert"><h5>{error}</h5></div> : ''}
          <Switch>
            <Route exact path="/">
              {urls.projects ? <ListProjects url={urls.projects.href} /> : ''}
            </Route>
            <Route path="/project/:id">
              {urls.projects ? <UpdateProvider><EditProject url={urls.projects.href} /></UpdateProvider> : ''}
            </Route>
          </Switch>
        </div>
      </HashRouter>
  );
};

function Global() {
  return (
    <ErrorProvider>
      <UrlProvider>
        <Main/>
      </UrlProvider>
    </ErrorProvider>
  );
}

ReactDOM.render(<Global />, document.getElementById('v-dom'));