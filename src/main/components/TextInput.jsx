import React, {useState} from 'react';
import axios from 'axios';

import {useUpdate} from 'hooks/useUpdate';
import {useErrorDispatch} from 'context/ErrorContext';
import {useUpdateDispatch} from 'context/UpdateContext';

export default function TextInput(props) {
  const setError = useErrorDispatch();
  const triggerChange = useUpdateDispatch();
  const [value, setValue] = useState(props.value);
  const name = props.name;
  const url = props.url;

  const type = props.type === 'form' ? 'form' : 'json';
  const method = props.method === 'post' ? 'post' : props.method === 'put' ? 'put' : 'patch';
  const placeholder = props.placeholder ? props.placeholder : '';
  const formGroupClass = 'form-group' + (props.className ? ' ' + props.className : '');

  const getData = {
    form: () => {
      let temp = new FormData();
      temp.append(name, value);
      return temp;
    },
    json: () => {
      return {[name]: value};
    }
  };

  useUpdate(() => {
    let timer = setTimeout(() => {
      axios[method](url, getData[type]())
        .then(() => triggerChange())
        .catch(e => setError('TextInput (useUpdate): ' + e.message));
    }, 1000);
    return () => clearTimeout(timer);
  }, [value]);

  return (
    <div className={formGroupClass}>
      {props.children}
      <input type="text" className="form-control" placeholder={placeholder} value={value} onChange={e => setValue(e.target.value)} />
    </div>
  );
};