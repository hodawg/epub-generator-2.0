import React, {useState} from 'react';
import axios from 'axios';

import {useErrorDispatch} from 'context/ErrorContext';
import {useUpdateDispatch} from 'context/UpdateContext';
import {useUpdate} from 'hooks/useUpdate';

export default function TextareaInput(props) {
  const setError = useErrorDispatch();
  const triggerChange = useUpdateDispatch();
  const [value, setValue] = useState(props.value);
  const name = props.name;
  const url = props.url;

  const method = props.method === 'post' ? 'post' : props.method === 'put' ? 'put' : 'patch';
  let rows = value.split('\n').length > 10 ? value.split('\n').length : 10;

  useUpdate(() => {
    let timer = setTimeout(() => {
      axios[method](url, {[name]: value})
        .then(() => triggerChange())
        .catch(e => setError('TextareaInput (useUpdate): ' + e.message));
    }, 1000);
    return () => clearTimeout(timer);
  }, [value]);

  return (
    <div className="form-group">
      {props.children}
      <textarea className="form-control" value={value} rows={rows} onChange={e => setValue(e.target.value)}></textarea>
    </div>
  );
};