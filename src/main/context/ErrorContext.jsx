import React, {useState} from 'react'

const ErrorStateContext = React.createContext();
const ErrorDispatchContext = React.createContext();

export function ErrorProvider({children}) {
  const [error, setError] = useState(null);
  return (
    <ErrorStateContext.Provider value={error}>
      <ErrorDispatchContext.Provider value={setError}>
        {children}
      </ErrorDispatchContext.Provider>
    </ErrorStateContext.Provider>
  );
}

export function useErrorState() {
  const context = React.useContext(ErrorStateContext);
  if (context === undefined) {
    throw new Error('useErrorState must be used within a ErrorProvider');
  }
  return context;
}

export function useErrorDispatch() {
  const context = React.useContext(ErrorDispatchContext);
  if (context === undefined) {
    throw new Error('useErrorDispatch must be used within a ErrorProvider');
  }
  return context;
}