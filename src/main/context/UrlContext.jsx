import React from 'react'

const UrlStateContext = React.createContext();
const UrlDispatchContext = React.createContext();

function urlReducer(state, action) {
  let filtered = Object.entries(action)
    .filter(([key, value]) => key !== 'profile' && key !== 'self');
  let newUrls = Object.fromEntries(filtered);
  return Object.assign({}, state, newUrls);
}

export function UrlProvider({children}) {
  const [urls, setUrls] = React.useReducer(urlReducer, {});

  return (
    <UrlStateContext.Provider value={urls}>
      <UrlDispatchContext.Provider value={setUrls}>
        {children}
      </UrlDispatchContext.Provider>
    </UrlStateContext.Provider>
  );
}

export function useUrlState() {
  const context = React.useContext(UrlStateContext);
  if (context === undefined) {
    throw new Error('useUrlState must be used within a UrlProvider');
  }
  return context;
}

export function useUrlDispatch() {
  const context = React.useContext(UrlDispatchContext);
  if (context === undefined) {
    throw new Error('useUrlDispatch must be used within a UrlProvider');
  }
  return context;
}