import React, {useState} from 'react'

const UpdateStateContext = React.createContext();
const UpdateDispatchContext = React.createContext();

export function UpdateProvider({children}) {
  const [state, setState] = useState(new Date());
  const trigger = () => setState(new Date());

  return (
    <UpdateStateContext.Provider value={state}>
      <UpdateDispatchContext.Provider value={trigger}>
        {children}
      </UpdateDispatchContext.Provider>
    </UpdateStateContext.Provider>
  );
}

export function useUpdateState() {
  const context = React.useContext(UpdateStateContext);
  if (context === undefined) {
    throw new Error('useUpdateState must be used within a UpdateProvider');
  }
  return context;
}

export function useUpdateDispatch() {
  const context = React.useContext(UpdateDispatchContext);
  if (context === undefined) {
    throw new Error('useUpdateDispatch must be used within a UpdateProvider');
  }
  return context;
}