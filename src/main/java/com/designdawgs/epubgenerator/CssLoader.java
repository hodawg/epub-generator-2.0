package com.designdawgs.epubgenerator;

import com.designdawgs.epubgenerator.domain.CssFile;
import com.designdawgs.epubgenerator.domain.repository.CssFileRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class CssLoader implements CommandLineRunner {
  private final CssFileRepository cssFileRepository;

  public CssLoader(CssFileRepository cssFileRepository) {
    this.cssFileRepository = cssFileRepository;
  }

  @Override
  public void run(String... args) throws Exception {
    if (cssFileRepository.count() == 0) {
      CssFile file = new CssFile();
      file.setCss("");
      cssFileRepository.save(file);
    }
  }
}