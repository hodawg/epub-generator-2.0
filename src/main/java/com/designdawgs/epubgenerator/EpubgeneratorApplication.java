package com.designdawgs.epubgenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EpubgeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(EpubgeneratorApplication.class, args);
	}
}
