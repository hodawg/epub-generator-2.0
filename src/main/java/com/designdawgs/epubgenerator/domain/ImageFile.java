package com.designdawgs.epubgenerator.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import org.springframework.hateoas.RepresentationModel;

@Entity
public class ImageFile extends RepresentationModel<ImageFile> implements Serializable {
  @Id
  @GeneratedValue
  private Long id;

  private String fileName;

  private String searchName;

  private int fileSize;

  private int width;

  private int height;

  @Lob
  @JsonIgnore
  private byte[] fileContent;

  private String mimeType;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public String getSearchName() {
    return searchName;
  }

  public void setSearchName(String searchName) {
    this.searchName = searchName;
  }

  public int getFileSize() {
    return fileSize;
  }

  public void setFileSize(int fileSize) {
    this.fileSize = fileSize;
  }

  public int getWidth() {
    return width;
  }

  public void setWidth(int width) {
    this.width = width;
  }

  public int getHeight() {
    return height;
  }

  public void setHeight(int height) {
    this.height = height;
  }

  public byte[] getFileContent() {
    return fileContent;
  }

  public void setFileContent(byte[] fileContent) {
    this.fileContent = fileContent;
  }

  public String getMimeType() {
    return mimeType;
  }

  public void setMimeType(String mimeType) {
    this.mimeType = mimeType;
  }

  @Override
  public String toString() {
    return "ImageFile{" + "id=" + id + ", fileName=" + fileName + ", searchName=" + searchName + ", fileSize=" + fileSize + ", width=" + width + ", height=" + height + ", mimeType=" + mimeType + '}';
  }
}