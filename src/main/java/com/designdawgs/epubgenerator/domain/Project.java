package com.designdawgs.epubgenerator.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import org.apache.commons.lang3.StringUtils;

@Entity
public class Project implements Serializable {
  @Id
  @GeneratedValue
  private Long id;

  private String title;

  private String author;

  private String volume;

  private String chapterTag;

  private String partTag;

  @Enumerated(EnumType.STRING)
  private FileEncoding encoding;

  private String imageAttr;

  @Column(length = 10000)
  private String notes;

  private Long coverId;

  @OneToOne(cascade = CascadeType.ALL)
  private TextFile textFile;

  @OneToMany(cascade = CascadeType.ALL)
  @JoinColumn(name="PROJECT_ID")
  private List<TagEdit> tagEdits = new ArrayList<>();

  @OneToMany(cascade = CascadeType.ALL)
  @JoinColumn(name="PROJECT_ID")
  private List<ImageFile> imageFiles = new ArrayList<>();

  public String getEpubTitle() {
    if (StringUtils.isNotBlank(volume)) {
      return title + " " + volume;
    }
    return title;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getAuthor() {
    if (author == null) {
      return "";
    }
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getAuthorLastFirst() {
    if (author == null) {
      return "";
    }
    String[] parts = author.split(" ");
    if (parts.length == 0) {
      return "";
    }
    if (parts.length == 1) {
      return parts[0];
    }
    if (parts.length == 2) {
      return parts[1] + ", " + parts[0];
    }
    return parts[2] + ", " + parts[0] + " " + parts[1];
  }

  public String getVolume() {
    return volume;
  }

  public void setVolume(String volume) {
    this.volume = volume;
  }

  public String getChapterTag() {
    return chapterTag;
  }

  public void setChapterTag(String chapterTag) {
    this.chapterTag = chapterTag;
  }

  public String getPartTag() {
    return partTag;
  }

  public void setPartTag(String partTag) {
    this.partTag = partTag;
  }

  public FileEncoding getEncoding() {
    return encoding;
  }

  public void setEncoding(FileEncoding encoding) {
    this.encoding = encoding;
  }

  public String getImageAttr() {
    return imageAttr;
  }

  public void setImageAttr(String imageAttr) {
    this.imageAttr = imageAttr;
  }

  public String getNotes() {
    return notes;
  }

  public void setNotes(String notes) {
    this.notes = notes;
  }

  public Long getCoverId() {
    return coverId;
  }

  public void setCoverId(Long coverId) {
    this.coverId = coverId;
  }

  public TextFile getTextFile() {
    return textFile;
  }

  public void setTextFile(TextFile textFile) {
    this.textFile = textFile;
  }

  public List<TagEdit> getTagEdits() {
    return tagEdits;
  }

  public void setTagEdits(List<TagEdit> tagEdits) {
    this.tagEdits = tagEdits;
  }

  public List<ImageFile> getImageFiles() {
    return imageFiles;
  }

  public void setImageFiles(List<ImageFile> imageFiles) {
    this.imageFiles = imageFiles;
  }

  @Override
  public int hashCode() {
    int hash = 7;
    hash = 83 * hash + Objects.hashCode(this.id);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Project other = (Project) obj;
    if (!Objects.equals(this.id, other.id)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "Project{" + "id=" + id + ", title=" + title + ", author=" + author + ", volume=" + volume + ", chapterTag=" + chapterTag + ", partTag=" + partTag + ", encoding=" + encoding + ", imageAttr=" + imageAttr + ", notes=" + notes + ", coverId=" + coverId + ", textFile=" + textFile + ", tagEdits=" + tagEdits + ", imageFiles=" + imageFiles + '}';
  }
}