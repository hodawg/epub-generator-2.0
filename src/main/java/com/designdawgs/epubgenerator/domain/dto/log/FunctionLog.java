package com.designdawgs.epubgenerator.domain.dto.log;

public class FunctionLog {
  private Integer id;
  private final String action;
  private final String level;
  private final String mutator;
  private final String message;

  FunctionLog(String action, String level, String mutator, String message) {
    this.action = action;
    this.level = level;
    this.mutator = mutator;
    this.message = message;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getAction() {
    return action;
  }

  public String getLevel() {
    return level;
  }

  public String getMutator() {
    return mutator;
  }

  public String getMessage() {
    return message;
  }

  @Override
  public String toString() {
    return level + " - " + action + " --- " + mutator + ": " + message;
  }
}