package com.designdawgs.epubgenerator.domain.dto.tag;

import java.util.List;
import java.util.stream.Collectors;
import org.jsoup.select.Elements;

public class AttributeInfo {
  private final String name;
  private final Integer count;
  private final List<String> values;

  public AttributeInfo(String tagName, String key, Elements elements) {
    this.name = key;
    this.count = elements.select(tagName + "[" + key + "]").size();
    this.values = elements.eachAttr(key).stream().distinct().collect(Collectors.toList());
  }

  public String getName() {
    return name;
  }

  public Integer getCount() {
    return count;
  }

  public List<String> getValues() {
    return values;
  }

  @Override
  public String toString() {
    return "AttributeResultDto{" + "name=" + name + ", count=" + count + ", values=" + values + '}';
  }
}