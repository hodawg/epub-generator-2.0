package com.designdawgs.epubgenerator.domain.dto.log;

public class FunctionLogger {
  private String action;
  private String mutator;

  private FunctionLogger() {}

  private FunctionLogger(String mutator, String action) {
    this.mutator = mutator;
    this.action = action;
  }

  public FunctionLog info(String message) {
    return new FunctionLog(action, "INFO", mutator, message);
  }

  public FunctionLog warning(String message) {
    return new FunctionLog(action, "WARNING", mutator, message);
  }

  public FunctionLog error(String message) {
    return new FunctionLog(action, "ERROR", mutator, message);
  }

  public static FunctionLogger getSelectLogger(String mutator) {
    return new FunctionLogger(mutator, "SELECT");
  }

  public static FunctionLogger getParamsLogger(String mutator) {
    return new FunctionLogger(mutator, "PARAMS");
  }

  public static FunctionLogger getInsertLogger(String mutator) {
    return new FunctionLogger(mutator, "INSERT");
  }

  public static FunctionLogger getReplaceLogger(String mutator) {
    return new FunctionLogger(mutator, "REPLACE");
  }

  public static FunctionLogger getRemoveLogger(String mutator) {
    return new FunctionLogger(mutator, "REMOVE");
  }
}