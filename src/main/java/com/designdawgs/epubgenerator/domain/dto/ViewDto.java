package com.designdawgs.epubgenerator.domain.dto;

import com.designdawgs.epubgenerator.domain.dto.log.FunctionLog;
import com.designdawgs.epubgenerator.domain.dto.tag.TagInfo;
import java.util.List;

public class ViewDto {
  private final List<FunctionLog> logs;
  private final List<TagInfo> tags;
  private final String preview;
  private final String source;

  public ViewDto(List<FunctionLog> log, List<TagInfo> tagResults, String preview, String source) {
    this.logs = log;
    this.tags = tagResults;
    this.preview = preview;
    this.source = source;
  }

  public List<FunctionLog> getLogs() {
    return logs;
  }

  public List<TagInfo> getTags() {
    return tags;
  }

  public String getPreview() {
    return preview;
  }

  public String getSource() {
    return source;
  }
}