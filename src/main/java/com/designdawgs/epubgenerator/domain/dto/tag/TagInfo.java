package com.designdawgs.epubgenerator.domain.dto.tag;

import java.util.List;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class TagInfo {
  private final String name;
  private final Integer count;
  private final List<AttributeInfo> attributes;

  public TagInfo(Elements elements) {
    this.name = elements.get(0).nodeName();
    this.count = elements.size();
    Function<Element, Stream<Attribute>> getAttributes = e -> e.attributes().asList().stream();
    Function<Attribute, String> getKeys = a -> a.getKey();
    Predicate<String> noSectionOrPart = s -> !"data-section".equals(s) && !"data-part".equals(s);
    List<String> attrKeys = elements.stream().flatMap(getAttributes).map(getKeys).filter(noSectionOrPart).distinct().collect(Collectors.toList());
    this.attributes = attrKeys.stream().map(k -> new AttributeInfo(elements.get(0).nodeName(), k, elements)).collect(Collectors.toList());
  }

  public String getName() {
    return name;
  }

  public Integer getCount() {
    return count;
  }

  public List<AttributeInfo> getAttributes() {
    return attributes;
  }

  @Override
  public String toString() {
    return "TagResultDto{" + "tagName=" + name + ", tagCount=" + count + ", attributes=" + attributes + '}';
  }
}