package com.designdawgs.epubgenerator.domain.function;

import com.designdawgs.epubgenerator.domain.dto.log.FunctionLog;
import com.designdawgs.epubgenerator.domain.dto.log.FunctionLogger;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class ReplaceElementConfig extends EditFunctionConfig {
  private final String HTML_STRING_TEMPLATE = "HTML_STRING_TEMPLATE";

  private final String label;
  private final Map<String, String> params;

  public ReplaceElementConfig() {
    this.label = "Replace Element";
    this.params = new LinkedHashMap<>();
    this.params.put(HTML_STRING_TEMPLATE, "html with %s");
  }

  @Override
  public String getLabel() {
    return label;
  }

  @Override
  public Map<String, String> getParams() {
    return params;
  }

  @Override
  public List<FunctionLog> execute(Elements elements, Map<String, String> params) {
    String template = params.get(HTML_STRING_TEMPLATE);

    Map<String, Long> counts = elements.stream()
      .map(e -> replaceElement(e, template))
      .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

    return counts.entrySet().stream().map(es -> generateLog(es.getKey(), es.getValue())).collect(Collectors.toList());
  }

  private String replaceElement(Element e, String template) {
    String newHtml = String.format(template, e.html());
    String oldHtml = e.outerHtml();
    e.after(newHtml);
    e.remove();
    return String.format("Replaced %1$s with %2$s", oldHtml, newHtml);
  }

  private FunctionLog generateLog(String element, Long count) {
    FunctionLogger logger = FunctionLogger.getReplaceLogger(label);
    String message = String.format("%s %d times.", element, count);
    return logger.info(message);
  }
}