package com.designdawgs.epubgenerator.domain.function;

import com.designdawgs.epubgenerator.domain.dto.log.FunctionLog;
import com.designdawgs.epubgenerator.domain.dto.log.FunctionLogger;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.jsoup.select.Elements;

public class RemoveElementConfig extends EditFunctionConfig {
  private final String label;
  private final Map<String, String> params;

  public RemoveElementConfig() {
    this.label = "Remove Element";
    this.params = new LinkedHashMap<>();
  }

  @Override
  public String getLabel() {
    return label;
  }

  @Override
  public Map<String, String> getParams() {
    return params;
  }

  @Override
  public List<FunctionLog> execute(Elements elements, Map<String, String> params) {
    Map<String, Long> counts = elements.stream()
      .map(e -> e.outerHtml())
      .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

    elements.remove();

    return counts.entrySet().stream().map(es -> generateLog(es.getKey(), es.getValue())).collect(Collectors.toList());
  }

  private FunctionLog generateLog(String element, Long count) {
    FunctionLogger logger = FunctionLogger.getRemoveLogger(label);
    String message = String.format("Removed %d occurrences of: %s", count, element);
    return logger.info(message);
  }
}