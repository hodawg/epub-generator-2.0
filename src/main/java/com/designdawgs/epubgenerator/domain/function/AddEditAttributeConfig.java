package com.designdawgs.epubgenerator.domain.function;

import com.designdawgs.epubgenerator.domain.dto.log.FunctionLog;
import com.designdawgs.epubgenerator.domain.dto.log.FunctionLogger;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class AddEditAttributeConfig extends EditFunctionConfig {
  private final String KEY = "KEY";
  private final String VALUE = "VALUE";

  private final String label;
  private final Map<String, String> params;

  public AddEditAttributeConfig() {
    this.label = "Add/Edit Attribute";
    this.params = new LinkedHashMap<>();
    this.params.put(KEY, "key");
    this.params.put(VALUE, "value");
  }

  @Override
  public String getLabel() {
    return label;
  }

  @Override
  public Map<String, String> getParams() {
    return params;
  }

  @Override
  public List<FunctionLog> execute(Elements elements, Map<String, String> params) {
    String key = params.get(KEY);
    String value = params.get(VALUE);

    Map<String, Long> counts = elements.stream()
      .map(e -> updateAttr(e, key, value))
      .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

    return counts.entrySet().stream()
      .map(es -> generateLog(es.getKey(), es.getValue()))
      .collect(Collectors.toList());
  }

  private String updateAttr(Element e, String key, String value) {
    if (e.hasAttr(key)) {
      String oldAttr = String.format("%1$s=%2$s", key, e.attr(key));
      e.attr(key, value);
      String newAttr = String.format("%1$s=%2$s", key, e.attr(key));
      return String.format("Replaced %1$s with %2$s", oldAttr, newAttr);
    }
    else {
      e.attr(key, value);
      return String.format("Added %1$s=%2$s", key, e.attr(key));
    }
  }

  private FunctionLog generateLog(String part, Long count) {
    FunctionLogger insertLogger = FunctionLogger.getInsertLogger(label);
    FunctionLogger replaceLogger = FunctionLogger.getReplaceLogger(label);
    
    String message = String.format("%s %d time(s).", part, count);

    if (part.startsWith("Replaced")) {
      return replaceLogger.info(message);
    }
    else {
      return insertLogger.info(message);
    }
  }
}