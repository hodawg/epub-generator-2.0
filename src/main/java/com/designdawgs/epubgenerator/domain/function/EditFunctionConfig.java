package com.designdawgs.epubgenerator.domain.function;

import com.designdawgs.epubgenerator.domain.dto.log.FunctionLog;
import java.util.List;
import java.util.Map;
import org.jsoup.select.Elements;

public abstract class EditFunctionConfig {
  public abstract String getLabel();
  public abstract Map<String, String> getParams();
  public abstract List<FunctionLog> execute(Elements elements, Map<String, String> params);
}