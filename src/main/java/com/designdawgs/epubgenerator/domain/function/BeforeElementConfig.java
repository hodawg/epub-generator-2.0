package com.designdawgs.epubgenerator.domain.function;

import com.designdawgs.epubgenerator.domain.dto.log.FunctionLog;
import com.designdawgs.epubgenerator.domain.dto.log.FunctionLogger;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class BeforeElementConfig extends EditFunctionConfig {
  private final String HTML = "HTML";

  private final String label;
  private final Map<String, String> params;

  public BeforeElementConfig() {
    this.label = "Before Element";
    this.params = new LinkedHashMap<>();
    this.params.put(HTML, "html");
  }

  @Override
  public String getLabel() {
    return label;
  }

  @Override
  public Map<String, String> getParams() {
    return params;
  }

  @Override
  public List<FunctionLog> execute(Elements elements, Map<String, String> params) {
    String html = params.get(HTML);
    Map<String, Long> counts = elements.stream()
      .map(e -> insertBefore(e, html))
      .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

    return counts.entrySet().stream()
      .map(es -> generateLog(es.getKey(), es.getValue()))
      .collect(Collectors.toList());
  }

  private String insertBefore(Element e, String html) {
    String beforeHtml = e.previousSibling() != null ? e.previousSibling().outerHtml(): "";
    e.before(html);

    if (e.previousSibling() != null && !beforeHtml.equals(e.previousSibling().outerHtml())) {
      return String.format("Added %1$s before %2$s", e.previousSibling().outerHtml(), e.outerHtml());
    }
    return String.format("Nothing was inserted before %s", e.outerHtml());
  }

  private FunctionLog generateLog(String message, Long count) {
    FunctionLogger logger = FunctionLogger.getInsertLogger(label);

    if (message.startsWith("Nothing")) {
      return logger.warning(String.format("%s on %d occasions.", message, count));
    }
    else {
      return logger.info(String.format("%s %d time(s).", message, count));
    }
  }
}