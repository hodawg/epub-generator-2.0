package com.designdawgs.epubgenerator.domain.function;

import com.designdawgs.epubgenerator.domain.dto.log.FunctionLog;
import com.designdawgs.epubgenerator.domain.dto.log.FunctionLogger;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.jsoup.select.Elements;

public class WrapConfig extends EditFunctionConfig {
  private final String HTML = "HTML";

  private final String label;
  private final Map<String, String> params;

  public WrapConfig() {
    this.label = "Wrap Tag";
    this.params = new LinkedHashMap<>();
    this.params.put(HTML, "html");
  }

  @Override
  public String getLabel() {
    return label;
  }

  @Override
  public Map<String, String> getParams() {
    return params;
  }

  @Override
  public List<FunctionLog> execute(Elements elements, Map<String, String> params) {
    String html = params.get(HTML);

    elements.wrap(html);

    FunctionLogger logger = FunctionLogger.getInsertLogger(label);
    return new ArrayList<>(List.of(logger.info(String.format("Added %s %d times.", html, elements.size()))));
  }
}