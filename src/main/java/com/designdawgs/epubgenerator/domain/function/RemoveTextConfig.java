package com.designdawgs.epubgenerator.domain.function;

import com.designdawgs.epubgenerator.domain.dto.log.FunctionLog;
import com.designdawgs.epubgenerator.domain.dto.log.FunctionLogger;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.MatchResult;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class RemoveTextConfig extends EditFunctionConfig {
  private final String REGEX = "REGEX";

  private final String label;
  private final Map<String, String> params;

  public RemoveTextConfig() {
    this.label = "Remove Text";
    this.params = new LinkedHashMap<>();
    params.put(REGEX, "(regex)");
  }

  @Override
  public String getLabel() {
    return label;
  }

  @Override
  public Map<String, String> getParams() {
    return params;
  }

  @Override
  public List<FunctionLog> execute(Elements elements, Map<String, String> params) {
    String regex = params.get(REGEX);

    Map<String, Long> counts = elements.stream()
      .flatMap(e -> removeText(e, regex).stream())
      .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

    return counts.entrySet().stream()
      .map(es -> generateLog(es.getKey(), es.getValue()))
      .collect(Collectors.toList());
  }

  private List<String> removeText(Element e, String regex) {
    List<String> matches = Pattern.compile(regex).matcher(e.html()).results().map(MatchResult::group).collect(Collectors.toList());
    matches.stream().forEachOrdered(m -> e.html(e.html().replace(m, "")));
    return matches;
  }

  private FunctionLog generateLog(String text, Long count) {
    FunctionLogger logger = FunctionLogger.getRemoveLogger(label);
    String message = String.format("Removed %d occurrences of %1s", count, text);
    return logger.info(message);
  }
}