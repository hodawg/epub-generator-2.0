package com.designdawgs.epubgenerator.domain.function;

import com.designdawgs.epubgenerator.domain.dto.log.FunctionLog;
import com.designdawgs.epubgenerator.domain.dto.log.FunctionLogger;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class SplitTextConfig extends EditFunctionConfig {
  private final String SPLIT_ON = "SPLIT_ON";
  private final String TEMPLATE = "TEMPLATE";

  private final String label;
  private final Map<String, String> params;

  public SplitTextConfig() {
    this.label = "Split Text";
    this.params = new LinkedHashMap<>();
    params.put(SPLIT_ON, "split on (html escaped)");
    params.put(TEMPLATE, "html with %1s, %2s...");
  }

  @Override
  public String getLabel() {
    return label;
  }

  @Override
  public Map<String, String> getParams() {
    return params;
  }

  @Override
  public List<FunctionLog> execute(Elements elements, Map<String, String> params) {
    String splitOn = params.get(SPLIT_ON);
    String template = params.get(TEMPLATE);

    Map<String, Long> counts = elements.stream()
      .map(e -> replaceHtml(e, splitOn, template))
      .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

    return counts.entrySet().stream().map(es -> generateLog(es.getKey(), es.getValue())).collect(Collectors.toList());
  }

  private String replaceHtml(Element e, String splitOn, String template) {
    String oldHtml = e.html();
    Object[] parts = e.html().split(splitOn);
    String newHtml = String.format(template, parts);

    e.html(newHtml);
    return String.format("Replaced %1$s with %2$s", oldHtml, newHtml);
  }

  private FunctionLog generateLog(String element, Long count) {
    FunctionLogger logger = FunctionLogger.getReplaceLogger(label);
    String message = String.format("%s %d times.", element, count);
    return logger.info(message);
  }
}