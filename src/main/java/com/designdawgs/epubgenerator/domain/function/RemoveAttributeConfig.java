package com.designdawgs.epubgenerator.domain.function;

import com.designdawgs.epubgenerator.domain.dto.log.FunctionLog;
import com.designdawgs.epubgenerator.domain.dto.log.FunctionLogger;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.jsoup.select.Elements;

public class RemoveAttributeConfig extends EditFunctionConfig {
  private final String ATTR_KEYS = "ATTR_KEYS";

  private final String label;
  private final Map<String, String> params;

  public RemoveAttributeConfig() {
    this.label = "Remove Attribute";
    this.params = new LinkedHashMap<>();
    params.put(ATTR_KEYS, "attribute keys...");
  }

  @Override
  public String getLabel() {
    return label;
  }

  @Override
  public Map<String, String> getParams() {
    return params;
  }

  @Override
  public List<FunctionLog> execute(Elements elements, Map<String, String> params) {
    List<String> attrKeys = Arrays.asList(params.get(ATTR_KEYS).split(", "));
    return attrKeys.stream().map(k -> removeAttr(k, elements)).collect(Collectors.toList());
  }

  private FunctionLog removeAttr(String attrKey, Elements elements) {
    FunctionLogger logger = FunctionLogger.getRemoveLogger(label);

    if (elements.hasAttr(attrKey)) {
      elements.removeAttr(attrKey);
      return logger.info(String.format("Removed attribute %s from %d elements.", attrKey, elements.size()));
    }
    else {
      return logger.warning(String.format("Attribute %s not found on %d selected elements.", attrKey, elements.size()));
    }
  }
}