package com.designdawgs.epubgenerator.domain.function;

import com.designdawgs.epubgenerator.domain.dto.log.FunctionLog;
import com.designdawgs.epubgenerator.domain.dto.log.FunctionLogger;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.jsoup.select.Elements;

public class StringTrimConfig extends EditFunctionConfig {
  private final String label;
  private final Map<String, String> params;

  public StringTrimConfig() {
    this.label = "String Trim";
    this.params = new LinkedHashMap<>();
  }

  @Override
  public String getLabel() {
    return label;
  }

  @Override
  public Map<String, String> getParams() {
    return params;
  }

  @Override
  public List<FunctionLog> execute(Elements elements, Map<String, String> params) {
    elements.forEach(element -> {
      element.html(element.html().trim());
    });
    FunctionLogger logger = FunctionLogger.getReplaceLogger(label);
    return new ArrayList<>(List.of(logger.info(String.format("Trimed %d strings.", elements.size()))));
  }
}