package com.designdawgs.epubgenerator.domain.function;

import com.designdawgs.epubgenerator.domain.dto.log.FunctionLog;
import com.designdawgs.epubgenerator.domain.dto.log.FunctionLogger;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class UnwrapConfig extends EditFunctionConfig {
  private final String label;
  private final Map<String, String> params;

  public UnwrapConfig() {
    this.label = "Unwrap";
    this.params = new LinkedHashMap<>();
  }

  @Override
  public String getLabel() {
    return label;
  }

  @Override
  public Map<String, String> getParams() {
    return params;
  }

  @Override
  public List<FunctionLog> execute(Elements elements, Map<String, String> params) {
    Map<String, Long> counts = elements.stream()
      .map(e -> buildTag(e))
      .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

    elements.unwrap();

    return counts.entrySet().stream()
      .map(es -> generateLog(es.getKey(), es.getValue()))
      .collect(Collectors.toList());
  }

  private String buildTag(Element e) {
    StringBuilder sb = new StringBuilder();
    sb.append("<").append(e.nodeName());
    if (e.attributes().size() > 0) {
      e.attributes().forEach(a -> {
        sb.append(" ").append(a.getKey()).append("=").append(a.getValue());
      });
    }
    sb.append(">");
    return sb.toString();
  }

  private FunctionLog generateLog(String text, Long count) {
    FunctionLogger logger = FunctionLogger.getRemoveLogger(label);
    String message = String.format("Unwrapped %d occurrences of %1s", count, text);
    return logger.info(message);
  }
}