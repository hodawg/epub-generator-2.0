package com.designdawgs.epubgenerator.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import org.springframework.hateoas.RepresentationModel;

@Entity
public class TextFile extends RepresentationModel<TextFile> implements Serializable {
  @Id
  @GeneratedValue
  private Long id;

  private String fileName;

  @Lob
  @JsonIgnore
  private byte[] fileContent;

  private String mimeType;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getFileName() {
    return fileName;
  }

  public void setFileName(String fileName) {
    this.fileName = fileName;
  }

  public byte[] getFileContent() {
    return fileContent;
  }

  public void setFileContent(byte[] fileContent) {
    this.fileContent = fileContent;
  }

  public String getMimeType() {
    return mimeType;
  }

  public void setMimeType(String mimeType) {
    this.mimeType = mimeType;
  }

  @Override
  public String toString() {
    return "TextFile{" + "id=" + id + ", fileName=" + fileName + ", mimeType=" + mimeType + '}';
  }
}