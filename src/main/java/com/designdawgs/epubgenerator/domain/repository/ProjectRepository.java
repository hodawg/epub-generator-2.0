package com.designdawgs.epubgenerator.domain.repository;

import com.designdawgs.epubgenerator.domain.Project;
import org.springframework.data.repository.CrudRepository;

public interface ProjectRepository extends CrudRepository<Project, Long> {

}