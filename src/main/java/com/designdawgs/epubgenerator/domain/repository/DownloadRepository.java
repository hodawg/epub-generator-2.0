package com.designdawgs.epubgenerator.domain.repository;

import com.designdawgs.epubgenerator.domain.Download;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RestResource;

@RestResource(exported = false)
public interface DownloadRepository extends JpaRepository<Download, Long> {

}