package com.designdawgs.epubgenerator.domain.repository;

import com.designdawgs.epubgenerator.domain.TagEdit;
import org.springframework.data.repository.CrudRepository;

public interface TagEditRepository extends CrudRepository<TagEdit, Long> {

}