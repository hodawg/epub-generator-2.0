package com.designdawgs.epubgenerator.domain.repository;

import com.designdawgs.epubgenerator.domain.ImageFile;
import org.springframework.data.repository.CrudRepository;

public interface ImageFileRepository extends CrudRepository<ImageFile, Long> {

}