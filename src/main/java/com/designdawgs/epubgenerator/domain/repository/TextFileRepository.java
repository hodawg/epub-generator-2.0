package com.designdawgs.epubgenerator.domain.repository;

import com.designdawgs.epubgenerator.domain.TextFile;
import org.springframework.data.repository.CrudRepository;

public interface TextFileRepository extends CrudRepository<TextFile, Long> {

}