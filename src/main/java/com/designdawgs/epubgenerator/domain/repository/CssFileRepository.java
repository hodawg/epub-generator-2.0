package com.designdawgs.epubgenerator.domain.repository;

import com.designdawgs.epubgenerator.domain.CssFile;
import org.springframework.data.repository.CrudRepository;

public interface CssFileRepository extends CrudRepository<CssFile, Long> {

}