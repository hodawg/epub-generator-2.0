package com.designdawgs.epubgenerator.domain.epub;

import java.util.ArrayList;
import java.util.List;
import org.jsoup.nodes.Element;

public class NavEntry {
  private final String name;
  private final String href;
  private final List<NavEntry> childEntries;

  public NavEntry(String name, String href) {
    this.name = name;
    this.href = href;
    this.childEntries = new ArrayList<>();
  }

  public Element getEntry() {
    Element li = new Element("li");
    li.appendChild(new Element("a").attr("href", href).text(name));

    if (!childEntries.isEmpty()) {
      Element ol = new Element("ol");
      childEntries.forEach(ne -> ol.appendChild(ne.getEntry()));
      li.appendChild(ol);
    }

    return li;
  }

  public List<NavEntry> getChildEntries() {
    return childEntries;
  }
}