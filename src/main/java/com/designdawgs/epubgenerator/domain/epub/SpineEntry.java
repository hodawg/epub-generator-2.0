package com.designdawgs.epubgenerator.domain.epub;

public class SpineEntry {
  private final String id;

  public SpineEntry(String id) {
    this.id = id;
  }

  public String getEntry() {
    return "    <itemref idref=\"" + id + "\"/>";
  }
}