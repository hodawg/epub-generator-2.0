package com.designdawgs.epubgenerator.domain.epub;

import org.apache.commons.lang3.StringUtils;

public class ManafestEntry {
  private final String id;
  private final String href;
  private final String mediaType;
  private final String properties;

  public ManafestEntry(String id, String href, String mediaType) {
    this.id = id;
    this.href = href;
    this.mediaType = mediaType;
    this.properties = null;
  }

  public ManafestEntry(String id, String href, String mediaType, String properties) {
    this.id = id;
    this.href = href;
    this.mediaType = mediaType;
    this.properties = properties;
  }

  public String getEntry() {
    StringBuilder builder = new StringBuilder();
    builder.append("    <item id=\"").append(id).append("\"");
    builder.append(" href=\"").append(href).append("\"");
    builder.append(" media-type=\"").append(mediaType).append("\"");

    if (StringUtils.isNotBlank(properties)) {
      builder.append(" properties=\"").append(properties).append("\"");
    }

    builder.append("/>");
    return builder.toString();
  }
}