package com.designdawgs.epubgenerator.domain.epub;

import java.util.ArrayList;
import java.util.List;

public class Epub {
  private List<ContentFile> contentFiles = new ArrayList<>();
  private List<NavEntry> navEntries = new ArrayList<>();
  private List<ManafestEntry> manafestEntries = new ArrayList<>();
  private List<SpineEntry> spineEntries = new ArrayList<>();
  private String coverId;

  public List<ContentFile> getContentFiles() {
    return contentFiles;
  }

  public void setContentFiles(List<ContentFile> contentFiles) {
    this.contentFiles = contentFiles;
  }

  public List<NavEntry> getNavEntries() {
    return navEntries;
  }

  public void setNavEntries(List<NavEntry> navEntries) {
    this.navEntries = navEntries;
  }

  public List<ManafestEntry> getManafestEntries() {
    return manafestEntries;
  }

  public void setManafestEntries(List<ManafestEntry> manafestEntries) {
    this.manafestEntries = manafestEntries;
  }

  public List<SpineEntry> getSpineEntries() {
    return spineEntries;
  }

  public void setSpineEntries(List<SpineEntry> spineEntries) {
    this.spineEntries = spineEntries;
  }

  public String getCoverId() {
    return coverId;
  }

  public void setCoverId(String coverId) {
    this.coverId = coverId;
  }
}