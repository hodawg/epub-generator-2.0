package com.designdawgs.epubgenerator.domain.epub;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import org.springframework.core.io.Resource;

public class ContentFile {
  private final String fileName;
  private final byte[] content;

  public ContentFile(String fileName, byte[] content) {
    this.fileName = fileName;
    this.content = content;
  }

  public ContentFile(String fileName, Resource file) {
    this.fileName = fileName;
    try {
      this.content = file.getInputStream().readAllBytes();
    }
    catch (IOException ex) {
      throw new RuntimeException(ex);
    }
  }

  public ContentFile(String fileName, String content) {
    this.fileName = fileName;
    this.content = content.getBytes(StandardCharsets.UTF_8);
  }

  public String getFileName() {
    return fileName;
  }

  public byte[] getContent() {
    return content;
  }
}