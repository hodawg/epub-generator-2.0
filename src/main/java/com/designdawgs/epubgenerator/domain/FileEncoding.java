package com.designdawgs.epubgenerator.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum FileEncoding {
  US_ASCII("ASCII", StandardCharsets.US_ASCII),
  ISO_8859_1("ISO 8859-1", StandardCharsets.ISO_8859_1),
  UTF_8("UTF-8", StandardCharsets.UTF_8),
  UTF_16BE("UTF-16 BE", StandardCharsets.UTF_16BE),
  UTF_16LE("UTF-16 LE", StandardCharsets.UTF_16LE),
  UTF_16("UTF-16", StandardCharsets.UTF_16);

  private final String label;
  private final Charset charset;

  private FileEncoding(String label, Charset charset) {
    this.label = label;
    this.charset = charset;
  }

  public String getName() {
    return this.name();
  }

  public String getLabel() {
    return label;
  }

  public Charset getCharset() {
    return charset;
  }
}