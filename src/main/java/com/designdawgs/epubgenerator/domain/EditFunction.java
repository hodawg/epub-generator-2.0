package com.designdawgs.epubgenerator.domain;

import com.designdawgs.epubgenerator.domain.dto.log.FunctionLog;
import com.designdawgs.epubgenerator.domain.function.AddEditAttributeConfig;
import com.designdawgs.epubgenerator.domain.function.AfterElementConfig;
import com.designdawgs.epubgenerator.domain.function.BeforeElementConfig;
import com.designdawgs.epubgenerator.domain.function.EditFunctionConfig;
import com.designdawgs.epubgenerator.domain.function.RemoveAttributeConfig;
import com.designdawgs.epubgenerator.domain.function.RemoveElementConfig;
import com.designdawgs.epubgenerator.domain.function.ReplaceElementConfig;
import com.designdawgs.epubgenerator.domain.function.RemoveTextConfig;
import com.designdawgs.epubgenerator.domain.function.SplitTextConfig;
import com.designdawgs.epubgenerator.domain.function.StringTrimConfig;
import com.designdawgs.epubgenerator.domain.function.UnwrapConfig;
import com.designdawgs.epubgenerator.domain.function.WrapConfig;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.List;
import java.util.Map;
import org.jsoup.select.Elements;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum EditFunction {
  WRAP(new WrapConfig()),
  UNWRAP(new UnwrapConfig()),
  BEFORE_ELEMENT(new BeforeElementConfig()),
  AFTER_ELEMENT(new AfterElementConfig()),
  REMOVE_ELEMENT(new RemoveElementConfig()),
  REPLACE_ELEMENT(new ReplaceElementConfig()),
  ADD_EDIT_ATTRIBUTE(new AddEditAttributeConfig()),
  REMOVE_ATTRIBUTE(new RemoveAttributeConfig()),
  REMOVE_TEXT(new RemoveTextConfig()),
  SPLIT_TEXT(new SplitTextConfig()),
  STRING_TRIM(new StringTrimConfig());

  private final EditFunctionConfig config;

  private EditFunction(EditFunctionConfig config) {
    this.config = config;
  }

  public String getName() {
    return this.name();
  }

  public String getLabel() {
    return config.getLabel();
  }

  public Map<String, String> getParams() {
    return config.getParams();
  }

  public List<FunctionLog> execute(Elements elements, Map<String, String> params) {
    return config.execute(elements, params);
  }
}