package com.designdawgs.epubgenerator.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import org.springframework.hateoas.RepresentationModel;

@Entity
public class TagEdit extends RepresentationModel<TagEdit> implements Serializable {
  @Id
  @GeneratedValue
  private Long id;

  private String selector;

  @Enumerated(EnumType.STRING)
  private EditFunction editFunction;

  @OneToMany(cascade = CascadeType.ALL)
  @JoinColumn(name="TAG_EDIT_ID")
  private List<TagParam> params = new ArrayList<>();

  private Boolean skip;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getSelector() {
    return selector;
  }

  public void setSelector(String selector) {
    this.selector = selector;
  }

  public EditFunction getEditFunction() {
    return editFunction;
  }

  public void setEditFunction(EditFunction editFunction) {
    this.editFunction = editFunction;
  }

  public List<TagParam> getParams() {
    return params;
  }

  public void setParams(List<TagParam> params) {
    this.params = params;
  }

  public Boolean getSkip() {
    return skip;
  }

  public void setSkip(Boolean skip) {
    this.skip = skip;
  }

  @Override
  public int hashCode() {
    int hash = 3;
    hash = 53 * hash + Objects.hashCode(this.id);
    return hash;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final TagEdit other = (TagEdit) obj;
    if (!Objects.equals(this.id, other.id)) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "TagEdit{" + "id=" + id + ", selector=" + selector + ", editFunction=" + editFunction + ", params=" + params + ", skip=" + skip + '}';
  }
}