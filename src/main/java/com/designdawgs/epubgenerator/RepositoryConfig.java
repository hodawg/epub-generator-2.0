package com.designdawgs.epubgenerator;

import com.designdawgs.epubgenerator.domain.ImageFile;
import com.designdawgs.epubgenerator.domain.Project;
import com.designdawgs.epubgenerator.domain.TagEdit;
import com.designdawgs.epubgenerator.domain.TextFile;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

@Configuration
public class RepositoryConfig implements RepositoryRestConfigurer {

  @Override
  public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
    config.exposeIdsFor(Project.class, TextFile.class, TagEdit.class, ImageFile.class);
  }
}