package com.designdawgs.epubgenerator.controllers;

import com.designdawgs.epubgenerator.domain.ImageFile;
import com.designdawgs.epubgenerator.domain.Project;
import com.designdawgs.epubgenerator.domain.repository.ProjectRepository;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class ImageController {
  private final ProjectRepository projectRepository;

  public ImageController(ProjectRepository projectRepository) {
    this.projectRepository = projectRepository;
  }

  @GetMapping("/project/{projectId}/image/{imageId}")
  public HttpEntity image(@PathVariable("projectId") Long projectId, @PathVariable("imageId") Long imageId) {
    Project project = projectRepository.findById(projectId).orElseThrow();
    ImageFile file = project.getImageFiles().stream().filter(f -> imageId.equals(f.getId())).findAny().orElseThrow();

    HttpHeaders header = new HttpHeaders();
    header.setContentType(MediaType.valueOf(file.getMimeType()));
    header.setContentLength(file.getFileContent().length);
    return new HttpEntity(file.getFileContent(), header);
  }
}