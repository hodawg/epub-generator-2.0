package com.designdawgs.epubgenerator.controllers;

import com.designdawgs.epubgenerator.domain.Download;
import com.designdawgs.epubgenerator.domain.repository.DownloadRepository;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class DownloadController {
  private final DownloadRepository downloadRepository;

  public DownloadController(DownloadRepository downloadRepository) {
    this.downloadRepository = downloadRepository;
  }

  @GetMapping("/download/{downloadId}")
  public HttpEntity download(@PathVariable("downloadId") Long downloadId) {
    Download download = downloadRepository.findById(downloadId).orElseThrow();
    downloadRepository.delete(download);

    HttpHeaders header = new HttpHeaders();
    header.setContentType(MediaType.valueOf(download.getMimeType()));
    header.set("Content-Disposition", "attachment; filename=\"" + download.getFileName() + "\"");
    header.setContentLength(download.getFileContent().length);
    return new HttpEntity(download.getFileContent(), header);
  }
}