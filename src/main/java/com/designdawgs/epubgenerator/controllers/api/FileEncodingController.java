package com.designdawgs.epubgenerator.controllers.api;

import com.designdawgs.epubgenerator.domain.FileEncoding;
import java.util.Arrays;
import java.util.List;
import org.springframework.data.rest.webmvc.RepositoryLinksResource;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.hateoas.server.RepresentationModelProcessor;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ExposesResourceFor(FileEncoding.class)
@RequestMapping("/api/fileEncodings")
public class FileEncodingController implements RepresentationModelProcessor<RepositoryLinksResource> {

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public List<FileEncoding> getFileEncodings() {
    return Arrays.asList(FileEncoding.values());
  }

  @Override
  public RepositoryLinksResource process(RepositoryLinksResource model) {
    model.add(linkTo(FileEncodingController.class).withRel("fileEncodings"));
    return model;
  }
}