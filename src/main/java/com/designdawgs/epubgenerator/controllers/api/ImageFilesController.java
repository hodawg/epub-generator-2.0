package com.designdawgs.epubgenerator.controllers.api;

import com.designdawgs.epubgenerator.domain.ImageFile;
import com.designdawgs.epubgenerator.domain.repository.ImageFileRepository;
import com.designdawgs.epubgenerator.service.FileService;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;

@RepositoryRestController
@RequestMapping("/api/imageFiles")
public class ImageFilesController {
  private final ImageFileRepository imageFileRepository;
  private final FileService fileService;

  public ImageFilesController(ImageFileRepository imageFileRepository, FileService fileService) {
    this.imageFileRepository = imageFileRepository;
    this.fileService = fileService;
  }

  @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<CollectionModel> postImageFiles(List<MultipartFile> files) {
    List<ImageFile> imageFiles = files.stream().map(f -> {
      return imageFileRepository.save(fileService.createImageFile(f));
    }).collect(Collectors.toList());

    List<EntityModel> responses = imageFiles.stream().map(f -> {
      return new EntityModel(f, linkTo(ImageFilesController.class).slash(f.getId()).withSelfRel());
    }).collect(Collectors.toList());

    Link selfLink = linkTo(ImageFilesController.class).withSelfRel();
    CollectionModel<EntityModel> response = new CollectionModel(responses, selfLink);
    return ResponseEntity.created(selfLink.toUri()).body(response);
  }

  @PatchMapping(value = "/{imageFileId}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<EntityModel> patchImageFile(@PathVariable("imageFileId") Long imageFileId, Optional<String> searchName, Optional<MultipartFile> file) {
    ImageFile imageFile = imageFileRepository.findById(imageFileId).orElseThrow();
    imageFile = imageFileRepository.save(fileService.patchImageFile(imageFile, searchName, file));
    Link selfLink = linkTo(ImageFilesController.class).slash(imageFile.getId()).withSelfRel();
    return ResponseEntity.ok(new EntityModel(imageFile, selfLink));
  }

  @DeleteMapping(value = "/{imageFileId}")
  @ResponseStatus(value = HttpStatus.NO_CONTENT)
  public void deleteImageFile(@PathVariable("imageFileId") Long imageFileId) {
    imageFileRepository.deleteById(imageFileId);
  }
}