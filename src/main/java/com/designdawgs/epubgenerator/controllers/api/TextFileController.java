package com.designdawgs.epubgenerator.controllers.api;

import com.designdawgs.epubgenerator.domain.TextFile;
import com.designdawgs.epubgenerator.domain.repository.TextFileRepository;
import com.designdawgs.epubgenerator.service.FileService;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

@RepositoryRestController
@RequestMapping("/api/textFiles")
public class TextFileController {
  private final TextFileRepository textFileRepository;
  private final FileService fileService;

  public TextFileController(TextFileRepository textFileRepository, FileService fileService) {
    this.textFileRepository = textFileRepository;
    this.fileService = fileService;
  }

  @PutMapping(value = "/{textFileId}", produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<EntityModel> putTextFile(@PathVariable("textFileId") Long textFileId, MultipartFile file) {
    TextFile textFile = textFileRepository.findById(textFileId).orElseThrow();
    textFile = textFileRepository.save(fileService.putTextFile(textFile, file));
    Link selfLink = linkTo(TextFileController.class).slash(textFile.getId()).withSelfRel();
    EntityModel<TextFile> response = new EntityModel(textFile, selfLink);
    return ResponseEntity.ok(response);
  }
}