package com.designdawgs.epubgenerator.controllers.api;

import com.designdawgs.epubgenerator.controllers.DownloadController;
import com.designdawgs.epubgenerator.domain.Download;
import com.designdawgs.epubgenerator.domain.Project;
import com.designdawgs.epubgenerator.domain.dto.ViewDto;
import com.designdawgs.epubgenerator.domain.repository.DownloadRepository;
import com.designdawgs.epubgenerator.domain.repository.ProjectRepository;
import com.designdawgs.epubgenerator.service.DocumentFactory;
import com.designdawgs.epubgenerator.service.DownloadFactory;
import org.jsoup.nodes.Document;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.RepresentationModel;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@RepositoryRestController
@RequestMapping(value = "/api/projects")
public class ProjectController {
  private final ProjectRepository projectRepository;
  private final DocumentFactory documentFactory;
  private final DownloadRepository downloadRepository;
  private final DownloadFactory downloadFactory;

  public ProjectController(ProjectRepository projectRepository, DocumentFactory documentFactory, DownloadRepository downloadRepository, DownloadFactory downloadFactory) {
    this.projectRepository = projectRepository;
    this.documentFactory = documentFactory;
    this.downloadRepository = downloadRepository;
    this.downloadFactory = downloadFactory;
  }

  @GetMapping("/{projectId}/scan")
  public ResponseEntity<EntityModel> scan(@PathVariable("projectId") Long projectId) {
    Project project = projectRepository.findById(projectId).orElseThrow();
    ViewDto results = documentFactory.getViewDto(project);
    Link link = linkTo(methodOn(ProjectController.class).scan(projectId)).withRel("scan");
    return ResponseEntity.ok(new EntityModel(results, link));
  }

  @GetMapping("/{projectId}/generate")
  public ResponseEntity<RepresentationModel> generate(@PathVariable("projectId") Long projectId) {
    Project project = projectRepository.findById(projectId).orElseThrow();
    Document doc = documentFactory.getDocument(project);
    Download download = downloadRepository.save(downloadFactory.generateDownload(project, doc));
    Link link = linkTo(DownloadController.class).slash("download").slash(download.getId()).withRel("download");
    return ResponseEntity.ok(new RepresentationModel(link));
  }
}