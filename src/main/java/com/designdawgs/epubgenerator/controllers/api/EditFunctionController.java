package com.designdawgs.epubgenerator.controllers.api;

import com.designdawgs.epubgenerator.domain.EditFunction;
import java.util.Arrays;
import java.util.List;
import org.springframework.data.rest.webmvc.RepositoryLinksResource;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.hateoas.server.RepresentationModelProcessor;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ExposesResourceFor(EditFunction.class)
@RequestMapping("/api/editFunctions")
public class EditFunctionController implements RepresentationModelProcessor<RepositoryLinksResource> {

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public List<EditFunction> getActionOptions() {
    return Arrays.asList(EditFunction.values());
  }

  @Override
  public RepositoryLinksResource process(RepositoryLinksResource model) {
    model.add(linkTo(EditFunctionController.class).withRel("editFunctions"));
    return model;
  }
}