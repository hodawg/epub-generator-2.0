package com.designdawgs.epubgenerator.controllers.api;

import com.designdawgs.epubgenerator.domain.Project;
import com.designdawgs.epubgenerator.domain.dto.StatDto;
import com.designdawgs.epubgenerator.domain.EditFunction;
import com.designdawgs.epubgenerator.domain.repository.ProjectRepository;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;
import org.springframework.data.rest.webmvc.RepositoryLinksResource;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.hateoas.server.RepresentationModelProcessor;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@ExposesResourceFor(StatDto.class)
@RequestMapping("/api/projectStats")
public class StatsController implements RepresentationModelProcessor<RepositoryLinksResource> {
  private final ProjectRepository projectRepository;

  public StatsController(ProjectRepository projectRepository) {
    this.projectRepository = projectRepository;
  }

  @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public List<StatDto> getStats() {
    List<Project> projects = StreamSupport.stream(projectRepository.findAll().spliterator(), false)
      .collect(Collectors.toList());
    List<EditFunction> usedFunctions = projects.stream()
      .flatMap(p -> p.getTagEdits().stream())
      .map(te -> te.getEditFunction())
      .collect(Collectors.toList());
    return Arrays.stream(EditFunction.values())
      .map(ef -> new StatDto(ef.getLabel(), Long.toString(usedFunctions.stream().filter(uf -> ef.equals(uf)).count())))
      .collect(Collectors.toList());
  }

  @Override
  public RepositoryLinksResource process(RepositoryLinksResource model) {
    model.add(linkTo(StatsController.class).withRel("projectStats"));
    return model;
  }
}