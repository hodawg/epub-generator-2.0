package com.designdawgs.epubgenerator.controllers.api;

import com.designdawgs.epubgenerator.domain.Project;
import com.designdawgs.epubgenerator.domain.TagEdit;
import com.designdawgs.epubgenerator.domain.TagParam;
import com.designdawgs.epubgenerator.domain.repository.ProjectRepository;
import com.designdawgs.epubgenerator.service.FileService;
import java.util.stream.Collectors;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(value = "/api/projects")
public class CopyProjectController {
  private final ProjectRepository projectRepository;
  private final FileService fileService;

  public CopyProjectController(ProjectRepository projectRepository, FileService fileService) {
    this.projectRepository = projectRepository;
    this.fileService = fileService;
  }

  @PostMapping("/{projectId}/copy")
  public ResponseEntity<EntityModel> copy(@PathVariable("projectId") Long projectId, Project project, MultipartFile file) {
    Project existing = projectRepository.findById(projectId).orElseThrow();
    project.setAuthor("unknown");
    project.setVolume("01");
    project.setChapterTag(existing.getChapterTag());
    project.setPartTag(existing.getPartTag());
    project.setEncoding(existing.getEncoding());
    project.setImageAttr(existing.getImageAttr());
    project.setNotes("");
    project.setTagEdits(existing.getTagEdits().stream().map(e -> newTagEdit(e)).collect(Collectors.toList()));
    project.setTextFile(fileService.createTextFile(file));
    project = projectRepository.save(project);
    Link selfLink = linkTo(ProjectController.class).slash(project.getId()).withSelfRel();
    EntityModel<Project> response = new EntityModel(project, selfLink);
    return ResponseEntity.ok(response);
  }

  private TagEdit newTagEdit(TagEdit existing) {
    TagEdit te = new TagEdit();
    te.setSelector(existing.getSelector());
    te.setEditFunction(existing.getEditFunction());
    te.setParams(existing.getParams().stream().map(p -> newParam(p)).collect(Collectors.toList()));
    te.setSkip(existing.getSkip());
    return te;
  }

  private TagParam newParam(TagParam p) {
    TagParam tp = new TagParam();
    tp.setKey(p.getKey());
    tp.setValue(p.getValue());
    return tp;
  }
}