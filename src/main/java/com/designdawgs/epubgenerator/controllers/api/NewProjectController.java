package com.designdawgs.epubgenerator.controllers.api;

import com.designdawgs.epubgenerator.domain.FileEncoding;
import com.designdawgs.epubgenerator.domain.Project;
import com.designdawgs.epubgenerator.domain.repository.ProjectRepository;
import com.designdawgs.epubgenerator.service.FileService;
import org.springframework.data.rest.webmvc.RepositoryLinksResource;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.server.RepresentationModelProcessor;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/projects/new")
public class NewProjectController implements RepresentationModelProcessor<RepositoryLinksResource> {
  private final ProjectRepository projectRepository;
  private final FileService fileService;

  public NewProjectController(ProjectRepository projectRepository, FileService fileService) {
    this.projectRepository = projectRepository;
    this.fileService = fileService;
  }

  @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<EntityModel> newProject(Project project, MultipartFile file) {
    project.setAuthor("unknown");
    project.setVolume("01");
    project.setChapterTag("h1");
    project.setPartTag("h2");
    project.setEncoding(FileEncoding.UTF_8);
    project.setImageAttr("src");
    project.setNotes("");
    project.setTextFile(fileService.createTextFile(file));
    project = projectRepository.save(project);
    Link selfLink = linkTo(ProjectController.class).slash(project.getId()).withSelfRel();
    EntityModel<Project> response = new EntityModel(project, selfLink);
    return ResponseEntity.ok(response);
  }

  @Override
  public RepositoryLinksResource process(RepositoryLinksResource model) {
    model.add(linkTo(NewProjectController.class).withRel("newProject"));
    return model;
  }
}