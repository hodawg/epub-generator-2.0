package com.designdawgs.epubgenerator.service;

import com.designdawgs.epubgenerator.domain.CssFile;
import com.designdawgs.epubgenerator.domain.Download;
import com.designdawgs.epubgenerator.domain.ImageFile;
import com.designdawgs.epubgenerator.domain.Project;
import com.designdawgs.epubgenerator.domain.epub.ContentFile;
import com.designdawgs.epubgenerator.domain.epub.Epub;
import com.designdawgs.epubgenerator.domain.epub.ManafestEntry;
import com.designdawgs.epubgenerator.domain.epub.NavEntry;
import com.designdawgs.epubgenerator.domain.epub.SpineEntry;
import com.designdawgs.epubgenerator.domain.repository.CssFileRepository;
import com.designdawgs.epubgenerator.service.download.DownloadFactoryUtils;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.Clock;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.UUID;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

@Service
public class DownloadFactoryImpl implements DownloadFactory {
  private final CssFileRepository cssFileRepository;
  private final DownloadFactoryUtils utils;

  public DownloadFactoryImpl(CssFileRepository cssFileRepository, DownloadFactoryUtils utils) {
    this.cssFileRepository = cssFileRepository;
    this.utils = utils;
  }

  @Override
  public Download generateDownload(Project project, Document document) {
    Epub epub = new Epub();
    getMimeType(epub);
    getContainer(epub);
    getStylesheet(epub);
    getImageFiles(epub, project.getImageFiles(), project.getCoverId());
    getContentPages(epub, project, document);
    getNavFile(epub, project.getEpubTitle());
    getMetadata(epub, project);

    Download download = new Download();
    download.setFileName(project.getEpubTitle().replace(" ", "_") + ".epub");
    download.setMimeType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
    download.setFileContent(utils.buildZipFile(epub.getContentFiles()));
    return download;
  }

  private void getMimeType(Epub epub) {
    epub.getContentFiles().add(new ContentFile("mimetype", "application/epub+zip"));
  }

  private void getContainer(Epub epub) {
    Resource resourceFile = new ClassPathResource("epub/container.xml", this.getClass().getClassLoader());
    epub.getContentFiles().add(new ContentFile("META-INF/container.xml", resourceFile));
  }

  private void getStylesheet(Epub epub) {
    CssFile cssFile = cssFileRepository.findAll().iterator().next();
    epub.getContentFiles().add(new ContentFile("css/stylesheet.css", cssFile.getCss()));
    epub.getManafestEntries().add(new ManafestEntry("css", "css/stylesheet.css", "text/css"));
  }

  private void getImageFiles(Epub epub, List<ImageFile> images, Long coverId) {
    for (int i = 0; i < images.size(); i++) {
      ImageFile image = images.get(i);
      String id = String.format("id_i%02d", i);
      String fileName = "images/" + image.getFileName();
      String mediaType = image.getMimeType();

      epub.getContentFiles().add(new ContentFile(fileName, image.getFileContent()));
      if (coverId.equals(image.getId())) {
        epub.setCoverId(id);
        epub.getManafestEntries().add(new ManafestEntry(id, fileName, mediaType, "cover-image"));
      }
      else {
        epub.getManafestEntries().add(new ManafestEntry(id, fileName, mediaType));
      }
    }
  }

  private void getContentPages(Epub epub, Project project, Document document) {
    String sectionNameTemplate = "content/section_%1$s.xhtml";
    String partNameTemplate = "content/section_%1$s_part_%2$s.xhtml";
    int index = 0;
    final Document doc = utils.addChapterData(document, project.getChapterTag(), project.getPartTag());

    Predicate<ImageFile> notInDoc = i -> doc.select("[src*=" + i.getFileName() + "]").isEmpty();
    List<ImageFile> startImages = project.getImageFiles().stream().filter(notInDoc).collect(Collectors.toList());
    for(ImageFile startImage : startImages) {
      Document fileContent = utils.generateImagePage(startImage, project.getEpubTitle());
      String fileName = String.format("content/illustration_%02d.xhtml", index);
      String id = String.format("id_c%02d", index);

      epub.getContentFiles().add(new ContentFile(fileName, "<?xml version='1.0' encoding='utf-8'?>\n" + fileContent.outerHtml()));
      epub.getManafestEntries().add(new ManafestEntry(id, fileName, "application/xhtml+xml"));
      epub.getSpineEntries().add(new SpineEntry(id));
      index++;
    }

    List<String> sections = doc.select("[data-section]").eachAttr("data-section").stream().distinct().sorted().collect(Collectors.toList());
    for(String section : sections) {
      Elements sectionElements = doc.select("[data-section=" + section + "]");
      List<String> parts = sectionElements.select("[data-part]").eachAttr("data-part").stream().distinct().sorted().collect(Collectors.toList());
      NavEntry sectionNavEntry = null;

      for(String part : parts) {
        Document fileContent = utils.generateContentPage(sectionElements.select("[data-part=" + part + "]"), project.getEpubTitle());
        String fileName = String.format(("00".equals(part) ? sectionNameTemplate : partNameTemplate), section, part);
        String id = String.format("id_c%02d", index);
        String navTitle = fileContent.body().selectFirst(":matchesOwn([a-zA-Z])").ownText();

        epub.getContentFiles().add(new ContentFile(fileName, "<?xml version='1.0' encoding='utf-8'?>\n" + fileContent.outerHtml()));
        epub.getManafestEntries().add(new ManafestEntry(id, fileName, "application/xhtml+xml"));
        epub.getSpineEntries().add(new SpineEntry(id));
        if (sectionNavEntry == null) {
          sectionNavEntry = new NavEntry(navTitle, fileName);
        }
        else {
          sectionNavEntry.getChildEntries().add(new NavEntry(navTitle, fileName));
        }
        index++;
      }
      epub.getNavEntries().add(sectionNavEntry);
    }
  }

  private void getNavFile(Epub epub, String title) {
    Document nav = utils.generateNavPage(title);
    epub.getNavEntries().forEach(ne -> nav.body().selectFirst("ol").appendChild(ne.getEntry()));
    String fileName = "nav.xhtml";

    epub.getContentFiles().add(new ContentFile(fileName, "<?xml version='1.0' encoding='utf-8'?>\n" + nav.outerHtml()));
    epub.getManafestEntries().add(new ManafestEntry("nav", fileName, "application/xhtml+xml", "nav"));
  }

  private void getMetadata(Epub epub, Project project) {
    try {
      Resource file = new ClassPathResource("epub/metadata.opf", this.getClass().getClassLoader());
      String template =  FileCopyUtils.copyToString(new InputStreamReader(file.getInputStream()));
      template = template.replace("{title}", project.getTitle() + " " + project.getVolume());
      template = template.replace("{authorA}", project.getAuthor());
      template = template.replace("{authorB}", project.getAuthorLastFirst());
      template = template.replace("{uuid}", UUID.randomUUID().toString());
      template = template.replace("{coverId}", epub.getCoverId());
      template = template.replace("{timestamp}", ZonedDateTime.now(Clock.systemUTC()).format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssX")));
      template = template.replace("{manifest}", epub.getManafestEntries().stream().map(me -> me.getEntry()).collect(Collectors.joining("\n")));
      template = template.replace("{spine}", epub.getSpineEntries().stream().map(se -> se.getEntry()).collect(Collectors.joining("\n")));
      epub.getContentFiles().add(new ContentFile("metadata.opf", template));
    }
    catch (IOException ex) {
      throw new RuntimeException(ex);
    }
  }
}