package com.designdawgs.epubgenerator.service;

import com.designdawgs.epubgenerator.controllers.ImageController;
import com.designdawgs.epubgenerator.domain.FileEncoding;
import com.designdawgs.epubgenerator.domain.ImageFile;
import com.designdawgs.epubgenerator.domain.Project;
import com.designdawgs.epubgenerator.domain.TextFile;
import com.designdawgs.epubgenerator.domain.dto.ViewDto;
import com.designdawgs.epubgenerator.domain.dto.log.FunctionLog;
import com.designdawgs.epubgenerator.domain.dto.tag.TagInfo;
import com.designdawgs.epubgenerator.service.document.EditorService;
import java.nio.charset.Charset;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.jsoup.select.Selector;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.stereotype.Service;

@Service
public class DocumentFactoryImpl implements DocumentFactory {
  private final EditorService editorService;

  public DocumentFactoryImpl(EditorService editorService) {
    this.editorService = editorService;
  }

  @Override
  public Document getDocument(Project project) {
    Document doc = parseTextFile(project.getTextFile(), project.getEncoding());
    processDocument(project, doc);
    return doc;
  }

  @Override
  public ViewDto getViewDto(Project project) {
    Document doc = parseTextFile(project.getTextFile(), project.getEncoding());
    List<FunctionLog> logs = processDocument(project, doc);
    List<TagInfo> tagScan = scanTags(doc);
    String source = escape(doc.select("body > *").toString());
    String preview = convertForPreview(project, doc);
    return new ViewDto(logs, tagScan, preview, source);
  }

  private Document parseTextFile(TextFile textFile, FileEncoding encoding) {
    byte[] contents = textFile.getFileContent();
    Charset charset = encoding.getCharset();
    Document doc = Jsoup.parse(new String(contents, charset));
    return doc;
  }

  private List<FunctionLog> processDocument(Project project, Document doc) {
    List<FunctionLog> logs = editorService.executeTags(project.getTagEdits(), doc);
    project.getImageFiles().stream().forEachOrdered(i -> executeImageSwap(i, doc, project.getImageAttr()));
    IntStream.range(0, logs.size()).forEachOrdered(i -> logs.get(i).setId(i));
    return logs;
  }

  private List<TagInfo> scanTags(Document doc) {
    List<String> tagNames = doc.select("body > *").select("*").stream().map(e -> e.nodeName()).distinct().collect(Collectors.toList());
    return tagNames.stream().map(t -> new TagInfo(doc.select(t))).collect(Collectors.toList());
  }

  private String escape(String text) {
    StringBuilder startTag = new StringBuilder();
    startTag.append("<");
    startTag.append("(?!code)");
    startTag.append("(?!del)");
    startTag.append("(?!ins)");
    startTag.append("(?!/code)");
    startTag.append("(?!/del)");
    startTag.append("(?!/ins)");

    StringBuilder endTag = new StringBuilder();
    endTag.append("(?<!code)");
    endTag.append("(?<!del)");
    endTag.append("(?<!ins)");
    endTag.append(">");

    text = text.replaceAll(startTag.toString(), "<code>&lt;");
    text = text.replaceAll(endTag.toString(), "&gt;</code>");
    return text;
  }

  private String convertForPreview(Project project, Document doc) {
    project.getImageFiles().stream().forEachOrdered(i -> executeImagePreview(i, doc, project.getId()));
    return doc.select("body > *").toString();
  }

  private void executeImageSwap(ImageFile imageFile, Document doc, String imageAttr) {
    if (StringUtils.isNotBlank(imageFile.getSearchName()) && StringUtils.isNotBlank(imageAttr)) {
      Element imageElement = new Element("img").attr("src", "../images/" + imageFile.getFileName()).attr("alt", imageFile.getFileName());
      String selector = String.format("[%1$s*=%2$s]", imageAttr, imageFile.getSearchName());
      try {
        Elements elements = doc.select(selector);
        elements.forEach(e -> e.replaceWith(imageElement));
      }
      catch (Selector.SelectorParseException | IllegalArgumentException ex) {

      }
    }
    else {

    }
  }

  private void executeImagePreview(ImageFile i, Document doc, Long id) {
    String link = linkTo(methodOn(ImageController.class).image(id, i.getId())).toUri().toString();
    doc.select("img[src*=" + i.getFileName() + "]").attr("src", link);
  }
}