package com.designdawgs.epubgenerator.service;

import com.designdawgs.epubgenerator.domain.Download;
import com.designdawgs.epubgenerator.domain.Project;
import org.jsoup.nodes.Document;

public interface DownloadFactory {
  public Download generateDownload(Project project, Document document);
}