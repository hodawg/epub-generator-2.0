package com.designdawgs.epubgenerator.service;

import com.designdawgs.epubgenerator.domain.Project;
import com.designdawgs.epubgenerator.domain.dto.ViewDto;
import org.jsoup.nodes.Document;

public interface DocumentFactory {
  public Document getDocument(Project project);
  public ViewDto getViewDto(Project project);
}