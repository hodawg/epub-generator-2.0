package com.designdawgs.epubgenerator.service.document;

import com.designdawgs.epubgenerator.domain.EditFunction;
import com.designdawgs.epubgenerator.domain.ImageFile;
import com.designdawgs.epubgenerator.domain.TagEdit;
import com.designdawgs.epubgenerator.domain.TagParam;
import com.designdawgs.epubgenerator.domain.dto.log.FunctionLog;
import com.designdawgs.epubgenerator.domain.dto.log.FunctionLogger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.jsoup.select.Selector;
import org.springframework.stereotype.Service;

@Service
public class EditorServiceImpl implements EditorService {

  @Override
  public List<FunctionLog> executeTags(List<TagEdit> edits, Document doc) {
    return edits.stream().flatMap(edit -> {
      List<FunctionLog> logs = new ArrayList<>();
      Elements elements = selectElements(edit, doc, logs);
      Map<String, String> params = getParams(edit, logs);
      return executeEdit(elements, edit.getEditFunction(), params, logs).stream();
    }).collect(Collectors.toList());
  }

  @Override
  public List<FunctionLog> executeImageSwaps(List<ImageFile> imageFiles, Document doc, String imageAttr) {
    List<FunctionLog> logs = new ArrayList<>();

    return logs;
  }

  private Elements selectElements(TagEdit edit, Document doc, List<FunctionLog> logs) {
    FunctionLogger logger = FunctionLogger.getSelectLogger(edit.getEditFunction().getLabel());

    if (StringUtils.isNotBlank(edit.getSelector()) && !Boolean.TRUE.equals(edit.getSkip())) {
      try {
        Elements selected = doc.select(edit.getSelector());

        if (selected != null && !selected.isEmpty()) {
          String message = String.format("Selected %d element(s) with '%s'.", selected.size(), edit.getSelector());
          logs.add(logger.info(message));
          return selected;
        }
        else {
          String message = String.format("No elements selected with '%s'.", edit.getSelector());
          logs.add(logger.warning(message));
        }
      }
      catch (Selector.SelectorParseException | IllegalArgumentException ex) {
        String message = String.format("'%1$s' errored with: %2$s", edit.getSelector(), ex.getMessage());
        logs.add(logger.error(message));
      }
    }
    return null;
  }

  private Map<String, String> getParams(TagEdit edit, List<FunctionLog> logs) {
    FunctionLogger logger = FunctionLogger.getParamsLogger(edit.getEditFunction().getLabel());

    Map<String, String> params = edit.getEditFunction().getParams().keySet().stream().map(key -> {
      Optional<TagParam> optional = edit.getParams().stream().filter(tp -> key.equals(tp.getKey())).findAny();

      if (optional.isPresent()) {
        return optional.get();
      }

      return new TagParam(key, null);
    }).collect(Collectors.toMap(TagParam::getKey, TagParam::getValue));

    if (params.isEmpty()) {
      logs.add(logger.info("No paramaters needed."));
      return params;
    }
    else if (params.values().stream().anyMatch(v -> StringUtils.isBlank(v))) {
      List<String> names = params.entrySet().stream()
        .filter(es -> es.getValue() == null)
        .map(es -> es.getKey().substring(0, 1).toUpperCase() + es.getKey().substring(1))
        .collect(Collectors.toList());

      logs.add(logger.error(String.format("Missing parameter(s) %s.", names.stream().collect(Collectors.joining(", ")))));
      return null;
    }
    else {
      List<String> pairs = params.entrySet().stream().map(es -> {
        return String.format("%1$s: %2$s", edit.getEditFunction().getParams().get(es.getKey()), es.getValue());
      }).collect(Collectors.toList());

      logs.add(logger.info(String.format("Using paramameter(s) %s", pairs.stream().collect(Collectors.joining(", ")))));
      return params;
    }
  }

  private List<FunctionLog> executeEdit(Elements elements, EditFunction function, Map<String, String> params, List<FunctionLog> logs) {
    if (elements != null && params != null) {
      logs.addAll(function.execute(elements, params));
    }
    return logs;
  }
}