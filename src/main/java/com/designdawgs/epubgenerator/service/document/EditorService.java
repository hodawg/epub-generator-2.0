package com.designdawgs.epubgenerator.service.document;

import com.designdawgs.epubgenerator.domain.ImageFile;
import com.designdawgs.epubgenerator.domain.TagEdit;
import com.designdawgs.epubgenerator.domain.dto.log.FunctionLog;
import java.util.List;
import org.jsoup.nodes.Document;

public interface EditorService {
  List<FunctionLog> executeTags(List<TagEdit> edits, Document doc);
  List<FunctionLog> executeImageSwaps(List<ImageFile> imageFiles, Document doc, String imageAttr);
}