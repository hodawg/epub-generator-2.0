package com.designdawgs.epubgenerator.service;

import com.designdawgs.epubgenerator.domain.ImageFile;
import com.designdawgs.epubgenerator.domain.TextFile;
import java.util.Optional;
import org.springframework.web.multipart.MultipartFile;

public interface FileService {
  public TextFile createTextFile(MultipartFile file);
  public TextFile putTextFile(TextFile existing, MultipartFile file);

  public ImageFile createImageFile(MultipartFile file);
  public ImageFile patchImageFile(ImageFile imageFile, Optional<String> searchName, Optional<MultipartFile> file);
}