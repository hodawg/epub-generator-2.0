package com.designdawgs.epubgenerator.service.download;

import com.designdawgs.epubgenerator.domain.ImageFile;
import com.designdawgs.epubgenerator.domain.epub.ContentFile;
import java.util.List;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

public interface DownloadFactoryUtils {
  public Document addChapterData(Document doc, String chapterTag, String partTag);
  public Document generateImagePage(ImageFile image, String title);
  public Document generateContentPage(Elements elements, String title);
  public Document generateNavPage(String title);

  public byte[] buildZipFile(List<ContentFile> content);
}