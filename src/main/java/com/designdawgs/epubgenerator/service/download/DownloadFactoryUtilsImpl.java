package com.designdawgs.epubgenerator.service.download;

import com.designdawgs.epubgenerator.domain.ImageFile;
import com.designdawgs.epubgenerator.domain.epub.ContentFile;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.stereotype.Service;

@Service
public class DownloadFactoryUtilsImpl implements DownloadFactoryUtils {

  @Override
  public Document addChapterData(Document doc, String chapterTag, String partTag) {
    if (StringUtils.isNotBlank(chapterTag)) {
      int chapter = 0;
      int part = 0;
      Elements all = doc.select("body > *");

      for(Element e : all) {
        if (e.is(chapterTag)) {
          chapter++;
          part = 0;
        }
        if (StringUtils.isNotBlank(partTag) && e.is(partTag)) {
          part++;
        }

        e.attr("data-section", String.format("%02d", chapter));
        e.attr("data-part", String.format("%02d", part));
      }
    }
    return doc;
  }

  @Override
  public Document generateImagePage(ImageFile image, String title) {
    Element imageElement = new Element("img").attr("src", "../images/" + image.getFileName()).attr("alt", image.getFileName());
    Document document = standardFilePrep(Document.createShell("na"), title);
    document.head().append("<link href=\"../css/stylesheet.css\" rel=\"stylesheet\" type=\"text/css\"/>");
    document.body().appendChild(new Element("div").appendChild(imageElement));
    return document;
  }

  @Override
  public Document generateContentPage(Elements elements, String title) {
    Document document = standardFilePrep(Jsoup.parse(elements.outerHtml()), title);
    document.head().append("<link href=\"../css/stylesheet.css\" rel=\"stylesheet\" type=\"text/css\"/>");
    document.select("body > *").removeAttr("data-section").removeAttr("data-part");
    return document;
  }

  @Override
  public Document generateNavPage(String title) {
    Document document = standardFilePrep(Document.createShell("na"), title);
    document.head().append("<link href=\"css/stylesheet.css\" rel=\"stylesheet\" type=\"text/css\"/>");
    document.body().appendChild(new Element("nav").attr("epub:type", "toc").appendChild(new Element("ol")));
    return document;
  }

  private Document standardFilePrep(Document fileContent, String title) {
    fileContent.outputSettings().syntax(Document.OutputSettings.Syntax.xml);
    fileContent.select("html").attr("xmlns:epub", "http://www.idpf.org/2007/ops").attr("xmlns", "http://www.w3.org/1999/xhtml");
    fileContent.title(title);
    return fileContent;
  }

  @Override
  public byte[] buildZipFile(List<ContentFile> content) {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();

    try (ZipOutputStream zos = new ZipOutputStream(baos)) {
      for(ContentFile f : content) {
        ZipEntry entry = new ZipEntry(f.getFileName());
        entry.setSize(f.getContent().length);
        zos.putNextEntry(entry);
        zos.write(f.getContent());
      }
      zos.closeEntry();
    }
    catch(IOException ex) {
      throw new RuntimeException(ex);
    }

    return baos.toByteArray();
  }
}