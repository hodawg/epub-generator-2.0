package com.designdawgs.epubgenerator.service;

import com.designdawgs.epubgenerator.domain.ImageFile;
import com.designdawgs.epubgenerator.domain.TextFile;
import java.awt.Dimension;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Optional;
import javax.imageio.ImageIO;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileServiceImpl implements FileService {

  @Override
  public TextFile createTextFile(MultipartFile file) {
    return setTextFile(new TextFile(), file);
  }

  @Override
  public TextFile putTextFile(TextFile existing, MultipartFile file) {
    return setTextFile(existing, file);
  }

  private TextFile setTextFile(TextFile textFile, MultipartFile file) {
    textFile.setFileName(file.getOriginalFilename());
    textFile.setFileContent(getContent(file));
    textFile.setMimeType(file.getContentType());
    return textFile;
  }

  @Override
  public ImageFile createImageFile(MultipartFile file) {
    Dimension dimensions = getImageDimension(file);
    byte[] content = getContent(file);

    ImageFile imageFile = new ImageFile();
    imageFile.setFileName(file.getOriginalFilename());
    imageFile.setSearchName(file.getOriginalFilename());
    imageFile.setFileSize(content.length);
    imageFile.setWidth(dimensions.width);
    imageFile.setHeight(dimensions.height);
    imageFile.setFileContent(content);
    imageFile.setMimeType(file.getContentType());

    return imageFile;
  }

  @Override
  public ImageFile patchImageFile(ImageFile imageFile, Optional<String> searchName, Optional<MultipartFile> file) {
    if (searchName.isPresent()) {
      imageFile.setSearchName(searchName.get());
    }
    if (file.isPresent()) {
      Dimension dimensions = getImageDimension(file.get());
      byte[] content = getContent(file.get());

      imageFile.setFileName(file.get().getOriginalFilename());
      imageFile.setFileSize(content.length);
      imageFile.setWidth(dimensions.width);
      imageFile.setHeight(dimensions.height);
      imageFile.setFileContent(content);
      imageFile.setMimeType(file.get().getContentType());
    }
    return imageFile;
  }

  private Dimension getImageDimension(MultipartFile file) {
    try {
      BufferedImage bimg = ImageIO.read(file.getInputStream());
      return new Dimension(bimg.getWidth(), bimg.getHeight());
    }
    catch (IOException ex) {
      return new Dimension();
    }
  }

  private byte[] getContent(MultipartFile file) {
    try {
      return file.getBytes();
    }
    catch(IOException ex) {
      return new byte[0];
    }
  }
}