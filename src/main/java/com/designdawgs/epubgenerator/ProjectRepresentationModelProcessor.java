package com.designdawgs.epubgenerator;

import com.designdawgs.epubgenerator.controllers.api.CopyProjectController;
import com.designdawgs.epubgenerator.controllers.api.ProjectController;
import com.designdawgs.epubgenerator.domain.Project;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelProcessor;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;
import org.springframework.stereotype.Component;

@Component
public class ProjectRepresentationModelProcessor implements RepresentationModelProcessor<EntityModel<Project>> {

  @Override
  public EntityModel<Project> process(EntityModel<Project> model) {
    model.add(linkTo(CopyProjectController.class).slash(model.getContent().getId()).slash("copy").withRel("copy"));
    model.add(linkTo(methodOn(ProjectController.class).scan(model.getContent().getId())).withRel("scan"));
    model.add(linkTo(methodOn(ProjectController.class).generate(model.getContent().getId())).withRel("generate"));
    return model;
  }
}