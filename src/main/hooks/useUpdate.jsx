import React, {useEffect, useRef} from 'react';

export const useUpdate = (fn, inputs) => {
  const didMount = useRef(false);

  useEffect(() => {
    if (didMount.current) {
      return fn();
    }
    else {
      didMount.current = true;
    }
  }, inputs);
};