package com.designdawgs.epubgenerator.domain.function;

import com.designdawgs.epubgenerator.domain.dto.log.FunctionLog;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class WrapConfigTest {
  private final EditFunctionConfig config = new WrapConfig();

  @Test
  public void wrapTest() {
    Document doc = Jsoup.parse("<p>Bob</p>");
    Elements elements = doc.select("p");
    Map<String, String> params = new HashMap<>();
    params.put("HTML", "<div>");

    List<FunctionLog> results = config.execute(elements, params);

    assertEquals("<div>\n <p>Bob</p>\n</div>", doc.body().html());
    assertEquals(1, results.size());
    assertEquals("INFO", results.get(0).getLevel());
    assertEquals("INSERT", results.get(0).getAction());
    assertEquals("Wrap Tag", results.get(0).getMutator());
  }
}