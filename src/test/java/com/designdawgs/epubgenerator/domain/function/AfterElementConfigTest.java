package com.designdawgs.epubgenerator.domain.function;

import com.designdawgs.epubgenerator.domain.dto.log.FunctionLog;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class AfterElementConfigTest {
  private final EditFunctionConfig config = new AfterElementConfig();

  @Test
  public void insertTextAfterTest() {
    Document doc = Jsoup.parse("<p>Bob</p>");
    Elements elements = doc.select("p");
    Map<String, String> params = new HashMap<>();
    params.put("HTML", "value");

    List<FunctionLog> results = config.execute(elements, params);

    assertEquals("<p>Bob</p>value", doc.body().html());
    assertEquals(1, results.size());
    assertEquals("INFO", results.get(0).getLevel());
    assertEquals("INSERT", results.get(0).getAction());
    assertEquals("After Element", results.get(0).getMutator());
  }

  @Test
  public void insertHtmlAfterTest() {
    Document doc = Jsoup.parse("<p>Bob</p>");
    Elements elements = doc.select("p");
    Map<String, String> params = new HashMap<>();
    params.put("HTML", "<p>value</p>");

    List<FunctionLog> results = config.execute(elements, params);

    assertEquals("<p>Bob</p>\n<p>value</p>", doc.body().html());
    assertEquals(1, results.size());
    assertEquals("INFO", results.get(0).getLevel());
    assertEquals("INSERT", results.get(0).getAction());
    assertEquals("After Element", results.get(0).getMutator());
  }

  @Test
  public void insertIncompleteTagAfterTest() {
    Document doc = Jsoup.parse("<p>Bob</p>");
    Elements elements = doc.select("p");
    Map<String, String> params = new HashMap<>();
    params.put("HTML", "<p");

    List<FunctionLog> results = config.execute(elements, params);

    assertEquals("<p>Bob</p>", doc.body().html());
    assertEquals(1, results.size());
    assertEquals("WARNING", results.get(0).getLevel());
    assertEquals("INSERT", results.get(0).getAction());
    assertEquals("After Element", results.get(0).getMutator());
  }

  @Test
  public void insertInvalidTagAfterTest() {
    Document doc = Jsoup.parse("<p>Bob</p>");
    Elements elements = doc.select("p");
    Map<String, String> params = new HashMap<>();
    params.put("HTML", "<bob>bob</bob>");

    List<FunctionLog> results = config.execute(elements, params);

    assertEquals("<p>Bob</p><bob>\n bob\n</bob>", doc.body().html());
    assertEquals(1, results.size());
    assertEquals("INFO", results.get(0).getLevel());
    assertEquals("INSERT", results.get(0).getAction());
    assertEquals("After Element", results.get(0).getMutator());
  }

  @Test
  public void insertAnotherInvalidTagAfterTest() {
    Document doc = Jsoup.parse("<p>Bob</p>");
    Elements elements = doc.select("p");
    Map<String, String> params = new HashMap<>();
    params.put("HTML", "<bobbob</bob>");

    List<FunctionLog> results = config.execute(elements, params);

    assertEquals("<p>Bob</p><bobbob></bobbob>", doc.body().html());
    assertEquals(1, results.size());
    assertEquals("INFO", results.get(0).getLevel());
    assertEquals("INSERT", results.get(0).getAction());
    assertEquals("After Element", results.get(0).getMutator());
  }

  @Test
  public void insertUnclosedTagAfterTest() {
    Document doc = Jsoup.parse("<p>Bob</p>");
    Elements elements = doc.select("p");
    Map<String, String> params = new HashMap<>();
    params.put("HTML", "<p>bob");

    List<FunctionLog> results = config.execute(elements, params);

    assertEquals("<p>Bob</p>\n<p>bob</p>", doc.body().html());
    assertEquals(1, results.size());
    assertEquals("INFO", results.get(0).getLevel());
    assertEquals("INSERT", results.get(0).getAction());
    assertEquals("After Element", results.get(0).getMutator());
  }

  @Test
  public void insertMultipleTest() {
    Document doc = Jsoup.parse("<p>Bob</p><p>Bob</p>");
    Elements elements = doc.select("p");
    Map<String, String> params = new HashMap<>();
    params.put("HTML", "<p>inserted</p>");

    List<FunctionLog> results = config.execute(elements, params);

    assertEquals("<p>Bob</p>\n<p>inserted</p>\n<p>Bob</p>\n<p>inserted</p>", doc.body().html());
    assertEquals(1, results.size());
    assertEquals("INFO", results.get(0).getLevel());
    assertEquals("INSERT", results.get(0).getAction());
    assertEquals("After Element", results.get(0).getMutator());
  }
}