package com.designdawgs.epubgenerator.domain.function;

import com.designdawgs.epubgenerator.domain.dto.log.FunctionLog;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class RemoveTextConfigTest {
  private final EditFunctionConfig config = new RemoveTextConfig();

  @Test
  public void dualGroupsTest() {
    Document doc = Jsoup.parse("<p>Bob Phil Bob Bob Phil</p>");
    Elements elements = doc.select("p");
    Map<String, String> params = new HashMap<>();
    params.put("REGEX", "(Bob) (Phil)");

    List<FunctionLog> results = config.execute(elements, params);

    assertEquals("<p>Bob</p>", doc.body().html());
    assertEquals(1, results.size());
    assertEquals("INFO", results.get(0).getLevel());
    assertEquals("REMOVE", results.get(0).getAction());
    assertEquals("Remove Text", results.get(0).getMutator());
  }

  @Test
  public void multipleTest() {
    Document doc = Jsoup.parse("<p>Bob Bob</p><p>Bob</p><p>Bob Bob</p>");
    Elements elements = doc.select("p");
    Map<String, String> params = new HashMap<>();
    params.put("REGEX", "(Bob)");

    List<FunctionLog> results = config.execute(elements, params);

    assertEquals("<p></p>\n<p></p>\n<p></p>", doc.body().html());
    assertEquals(1, results.size());
    assertEquals("INFO", results.get(0).getLevel());
    assertEquals("REMOVE", results.get(0).getAction());
    assertEquals("Remove Text", results.get(0).getMutator());
  }
}