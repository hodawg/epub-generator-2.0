package com.designdawgs.epubgenerator.domain.function;

import com.designdawgs.epubgenerator.domain.dto.log.FunctionLog;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class AddEditAttributeConfigTest {
  private final EditFunctionConfig config = new AddEditAttributeConfig();

  @Test
  public void addAttributeTest() {
    Element element = new Element("p");
    Elements elements = new Elements(new ArrayList<>(List.of(element)));
    Map<String, String> params = new HashMap<>();
    params.put("KEY", "class");
    params.put("VALUE", "bob");

    List<FunctionLog> results = config.execute(elements, params);

    assertEquals(1, element.attributes().size());
    assertEquals("bob", element.attr("class"));
    assertEquals(1, results.size());
    assertEquals("INFO", results.get(0).getLevel());
    assertEquals("INSERT", results.get(0).getAction());
    assertEquals("Add/Edit Attribute", results.get(0).getMutator());
  }

  @Test
  public void changeAttributeTest() {
    Element element = new Element("p").attr("class", "phil").attr("data-bob", "phil");
    Elements elements = new Elements(new ArrayList<>(List.of(element)));
    Map<String, String> params = new HashMap<>();
    params.put("KEY", "class");
    params.put("VALUE", "bob");

    List<FunctionLog> results = config.execute(elements, params);
    assertEquals(2, element.attributes().size());
    assertEquals("bob", element.attr("class"));
    assertEquals("phil", element.attr("data-bob"));
    assertEquals(1, results.size());
    assertEquals("INFO", results.get(0).getLevel());
    assertEquals("REPLACE", results.get(0).getAction());
    assertEquals("Add/Edit Attribute", results.get(0).getMutator());
  }

  @Test
  public void invalidAttributeTest() {
    Element element = new Element("p");
    Elements elements = new Elements(new ArrayList<>(List.of(element)));
    Map<String, String> params = new HashMap<>();
    params.put("KEY", "bob");
    params.put("VALUE", "bob");

    List<FunctionLog> results = config.execute(elements, params);

    assertEquals(1, element.attributes().size());
    assertEquals("bob", element.attr("bob"));
    assertEquals(1, results.size());
    assertEquals("INFO", results.get(0).getLevel());
    assertEquals("INSERT", results.get(0).getAction());
    assertEquals("Add/Edit Attribute", results.get(0).getMutator());
  }

  @Test
  public void addOneChangeOneTest() {
    Element element1 = new Element("p").attr("class", "phil");
    Element element2 = new Element("p");
    Elements elements = new Elements(new ArrayList<>(List.of(element1, element2)));
    Map<String, String> params = new HashMap<>();
    params.put("KEY", "class");
    params.put("VALUE", "bob");

    List<FunctionLog> results = config.execute(elements, params);
    assertEquals(1, element1.attributes().size());
    assertEquals("bob", element1.attr("class"));
    assertEquals(1, element2.attributes().size());
    assertEquals("bob", element2.attr("class"));
    assertEquals(2, results.size());
    assertEquals("INFO", results.get(0).getLevel());
    assertEquals("INSERT", results.get(0).getAction());
    assertEquals("Add/Edit Attribute", results.get(0).getMutator());
    assertEquals("INFO", results.get(1).getLevel());
    assertEquals("REPLACE", results.get(1).getAction());
    assertEquals("Add/Edit Attribute", results.get(1).getMutator());
  }
}