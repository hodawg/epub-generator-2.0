package com.designdawgs.epubgenerator.domain.function;

import com.designdawgs.epubgenerator.domain.dto.log.FunctionLog;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class RemoveAttributeConfigTest {
  private final EditFunctionConfig config = new RemoveAttributeConfig();

  @Test
  public void removeAttrTest() {
    Document doc = Jsoup.parse("<p class=\"value\">Bob</p>");
    Elements elements = doc.select("p");
    Map<String, String> params = new HashMap<>();
    params.put("ATTR_KEYS", "class");

    List<FunctionLog> results = config.execute(elements, params);

    assertEquals("<p>Bob</p>", doc.body().html());
    assertEquals(1, results.size());
    assertEquals("INFO", results.get(0).getLevel());
    assertEquals("REMOVE", results.get(0).getAction());
    assertEquals("Remove Attribute", results.get(0).getMutator());
  }

  @Test
  public void removeMultipleAttrTest() {
    Document doc = Jsoup.parse("<p class=\"value\" data-bob=\"value\">Bob</p>");
    Elements elements = doc.select("p");
    Map<String, String> params = new HashMap<>();
    params.put("ATTR_KEYS", "class, data-bob");

    List<FunctionLog> results = config.execute(elements, params);

    assertEquals("<p>Bob</p>", doc.body().html());
    assertEquals(2, results.size());
    assertEquals("INFO", results.get(0).getLevel());
    assertEquals("REMOVE", results.get(0).getAction());
    assertEquals("Remove Attribute", results.get(0).getMutator());
    assertEquals("INFO", results.get(1).getLevel());
    assertEquals("REMOVE", results.get(1).getAction());
    assertEquals("Remove Attribute", results.get(1).getMutator());
  }

  @Test
  public void partialMatchTest() {
    Document doc = Jsoup.parse("<p class=\"value\">Bob</p>");
    Elements elements = doc.select("p");
    Map<String, String> params = new HashMap<>();
    params.put("ATTR_KEYS", "cla");

    List<FunctionLog> results = config.execute(elements, params);

    assertEquals("<p class=\"value\">Bob</p>", doc.body().html());
    assertEquals(1, results.size());
    assertEquals("WARNING", results.get(0).getLevel());
    assertEquals("REMOVE", results.get(0).getAction());
    assertEquals("Remove Attribute", results.get(0).getMutator());
  }
}