package com.designdawgs.epubgenerator.domain.function;

import com.designdawgs.epubgenerator.domain.dto.log.FunctionLog;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class StringTrimConfigTest {
  private final EditFunctionConfig config = new StringTrimConfig();

  @Test
  public void trimTextTest() {
    Document doc = Jsoup.parse("<p>Bob </p>");
    Elements elements = doc.select("p");
    Map<String, String> params = new HashMap<>();

    List<FunctionLog> results = config.execute(elements, params);

    assertEquals("<p>Bob</p>", doc.body().html());
    assertEquals(1, results.size());
    assertEquals("INFO", results.get(0).getLevel());
    assertEquals("REPLACE", results.get(0).getAction());
    assertEquals("String Trim", results.get(0).getMutator());
  }
}