package com.designdawgs.epubgenerator.domain.function;

import com.designdawgs.epubgenerator.domain.dto.log.FunctionLog;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class SplitTextConfigTest {
  private final EditFunctionConfig config = new SplitTextConfig();

  @Test
  public void splitTextTest() {
    Document doc = Jsoup.parse("<p>Chapter 1: Bob >> Bob Again</p>");
    Elements elements = doc.select("p:containsOwn(: )");
    Map<String, String> params = new HashMap<>();
    params.put("SPLIT_ON", ": ");
    params.put("TEMPLATE", "<h1>%1s</h1><h4>%2s</h4>");

    List<FunctionLog> results = config.execute(elements, params);

    assertEquals("<p><h1>Chapter 1</h1><h4>Bob &gt;&gt; Bob Again</h4></p>", doc.body().html());
    assertEquals(1, results.size());
    assertEquals("INFO", results.get(0).getLevel());
    assertEquals("REPLACE", results.get(0).getAction());
    assertEquals("Split Text", results.get(0).getMutator());
  }

  @Test
  public void splitText2Test() {
    Document doc = Jsoup.parse("<p>Chapter 1: Bob >> Bob Again</p>");
    Elements elements = doc.select("p:containsOwn( >> )");
    Map<String, String> params = new HashMap<>();
    params.put("SPLIT_ON", " &gt;&gt; ");
    params.put("TEMPLATE", "<h1>%1s</h1><h4>%2s</h4>");

    List<FunctionLog> results = config.execute(elements, params);

    assertEquals("<p><h1>Chapter 1: Bob</h1><h4>Bob Again</h4></p>", doc.body().html());
    assertEquals(1, results.size());
    assertEquals("INFO", results.get(0).getLevel());
    assertEquals("REPLACE", results.get(0).getAction());
    assertEquals("Split Text", results.get(0).getMutator());
  }
}