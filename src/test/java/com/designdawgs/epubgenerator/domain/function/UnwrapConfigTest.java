package com.designdawgs.epubgenerator.domain.function;

import com.designdawgs.epubgenerator.domain.dto.log.FunctionLog;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class UnwrapConfigTest {
  private final EditFunctionConfig config = new UnwrapConfig();

  @Test
  public void unwrapTest() {
    Document doc = Jsoup.parse("<div><p>Bob</p></div>");
    Elements elements = doc.select("div");
    Map<String, String> params = new HashMap<>();

    List<FunctionLog> results = config.execute(elements, params);

    assertEquals("<p>Bob</p>", doc.body().html());
    assertEquals(1, results.size());
    assertEquals("INFO", results.get(0).getLevel());
    assertEquals("REMOVE", results.get(0).getAction());
    assertEquals("Unwrap", results.get(0).getMutator());
  }
}