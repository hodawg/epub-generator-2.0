package com.designdawgs.epubgenerator.domain.function;

import com.designdawgs.epubgenerator.domain.dto.log.FunctionLog;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class ReplaceElementConfigTest {
  private final EditFunctionConfig config = new ReplaceElementConfig();

  @Test
  public void replaceElementTest() {
    Document doc = Jsoup.parse("<p>Bob</p>");
    Elements elements = doc.select("p");
    Map<String, String> params = new HashMap<>();
    params.put("HTML_STRING_TEMPLATE", "<h1>%s</h1>");

    List<FunctionLog> results = config.execute(elements, params);

    assertEquals("<h1>Bob</h1>", doc.body().html());
    assertEquals(1, results.size());
    assertEquals("INFO", results.get(0).getLevel());
    assertEquals("REPLACE", results.get(0).getAction());
    assertEquals("Replace Element", results.get(0).getMutator());
  }

  @Test
  public void replaceElementWithHtml1Test() {
    Document doc = Jsoup.parse("<p>Bob<br/><br/></p>");
    Elements elements = doc.select("p");
    Map<String, String> params = new HashMap<>();
    params.put("HTML_STRING_TEMPLATE", "<h1>%s</h1>");

    List<FunctionLog> results = config.execute(elements, params);

    assertEquals("<h1>Bob<br><br></h1>", doc.body().html());
    assertEquals(1, results.size());
    assertEquals("INFO", results.get(0).getLevel());
    assertEquals("REPLACE", results.get(0).getAction());
    assertEquals("Replace Element", results.get(0).getMutator());
  }

  @Test
  public void replaceElementWithHtml2Test() {
    Document doc = Jsoup.parse("<p>This is <i>Bob</i></p>");
    Elements elements = doc.select("p");
    Map<String, String> params = new HashMap<>();
    params.put("HTML_STRING_TEMPLATE", "<h1>%s</h1>");

    List<FunctionLog> results = config.execute(elements, params);

    assertEquals("<h1>This is <i>Bob</i></h1>", doc.body().html());
    assertEquals(1, results.size());
    assertEquals("INFO", results.get(0).getLevel());
    assertEquals("REPLACE", results.get(0).getAction());
    assertEquals("Replace Element", results.get(0).getMutator());
  }

  @Test
  public void replceElementWithMultipleTest() {
    Document doc = Jsoup.parse("<dd>Bob<br><br><em>Bob</em></dd>");
    Elements elements = doc.select("dd");
    Map<String, String> params = new HashMap<>();
    params.put("HTML_STRING_TEMPLATE", "<p><br></p><p>%s</p><p><br></p>");

    List<FunctionLog> results = config.execute(elements, params);

    assertEquals("<p><br></p>\n<p>Bob <br> <br><em>Bob</em></p>\n<p><br></p>", doc.body().html());
    assertEquals(1, results.size());
    assertEquals("INFO", results.get(0).getLevel());
    assertEquals("REPLACE", results.get(0).getAction());
    assertEquals("Replace Element", results.get(0).getMutator());
  }
}