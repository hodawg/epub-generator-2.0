package com.designdawgs.epubgenerator.domain.function;

import com.designdawgs.epubgenerator.domain.dto.log.FunctionLog;
import java.util.List;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.Test;

public class RemoveElementConfigTest {
  private final EditFunctionConfig config = new RemoveElementConfig();

  @Test
  public void removeOneTest() {
    Document doc = Jsoup.parse("<h1>Title</h1><p>Bob</p>");
    Elements elements = doc.select("p");

    List<FunctionLog> results = config.execute(elements, null);

    assertEquals("<h1>Title</h1>", doc.body().html());
    assertEquals(1, results.size());
    assertEquals("INFO", results.get(0).getLevel());
    assertEquals("REMOVE", results.get(0).getAction());
    assertEquals("Remove Element", results.get(0).getMutator());
  }

  @Test
  public void removeMultipleTest() {
    Document doc = Jsoup.parse("<h1>Title</h1><p>Bob</p><p>Bob</p><p>Phill</p><p>Frank</p>");
    Elements elements = doc.select("p");

    List<FunctionLog> results = config.execute(elements, null);

    assertEquals("<h1>Title</h1>", doc.body().html());
    assertEquals(3, results.size());
    assertEquals("INFO", results.get(0).getLevel());
    assertEquals("REMOVE", results.get(0).getAction());
    assertEquals("Remove Element", results.get(0).getMutator());
    assertEquals("INFO", results.get(1).getLevel());
    assertEquals("REMOVE", results.get(1).getAction());
    assertEquals("Remove Element", results.get(1).getMutator());
    assertEquals("INFO", results.get(2).getLevel());
    assertEquals("REMOVE", results.get(2).getAction());
    assertEquals("Remove Element", results.get(2).getMutator());
  }
}